## Setup
npm run setup_npm (installazioni globali)

npm run setup_project (installazioni dipendenze progetto)

## Starter
PS: vedi i percorsi in config/start-android-* da sostituire con il vostro setup
per avviare tutto --> npm run all e farà partire :
- React devServer
- Emulatore Android
- Android dev

## NB
magari darà problemi con iOS perchè ho usato una macchina virtuale con Catalina che ha parametri diversi (situazione temporanea fino aquando mi arriva il mio iMac)
ma su android dovrebbe essere tutto funzionante
