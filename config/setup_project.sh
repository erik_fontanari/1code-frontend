#!/bin/sh

echo "--- INSTALLAZIONE DIPENDENZE PROGETTO ---"
rm -R node_modules
rm package-lock.json
npm install
react-native link
react-native link react-native-svg
