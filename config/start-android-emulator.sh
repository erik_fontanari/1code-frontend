#!/bin/sh

emaulatorName=Pixel_3_XL_API_28
rm /home/macchine/.android/avd/${emaulatorName}.avd/*.lock
cd ~/Android/Sdk/emulator
echo "--- EMULATORS AVAILABLE -------------------------------------------------"
./emulator -avd -list-avds

echo "--- START ANDROID EMULATOR ${emaulatorName} -----------------------------"
./emulator -avd ${emaulatorName}
