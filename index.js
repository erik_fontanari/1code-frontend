/**
 * @format
 */

import {AppRegistry} from 'react-native';
import MainContent from './src/app/';
// import MainContent from './src/';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => MainContent);
