import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Container, Content } from 'native-base';

import { styles } from './App-Styles';
import { COMPONENT_SETTINGS } from './App-Settings';
import { VIEWS_SETTINGS } from '../../config/app';

import Navbar from '../sections/navbar/';
import LoadingProto from '../sections/loading/';
import HomeSelection from '../views/unlogged/home-selection/';
import RegisterUser from '../views/unlogged/register-user/';
import AccessUser from '../views/unlogged/access-user/';
import HomeLogged from '../views/logged/home-logged/';

export default class AppComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor( props ) {
    super( props );
  }

  componentDidMount(){
    this.props.loadingData();
	}

  UNSAFE_componentWillReceiveProps( nextProps ) {
    if( nextProps.updateOn !== this.state.updateOn ) {
      this.setState( nextProps );
    }
  }

  render(){

    let modeView;

    if( this.state.user ) {

      // un-logged
      if( this.state.user.logged === false ) {

        if( this.state.localView.name === "init" ) {
          modeView = (
            <HomeSelection />
          )
        }

        if( this.state.localView.name === "registration" ) {
          modeView = (
            <View style={{...styles.mainContentSection,flex:1}}>
              <RegisterUser localView={ this.state.localView } updateOn={ this.state.updateOn } />
            </View>
          )
        }

        if( this.state.localView.name === "access" ) {
          modeView = (
            <AccessUser localView={ this.state.localView } updateOn={ this.state.updateOn } />
          )
        }
      }

      // logged
      else {
        modeView = (
          <View style={ styles.mainContentSection }>
            <HomeLogged />
          </View>
        )
      }
    }

    return (
      <View style={styles.container}>

        { !this.state.user &&
          <LoadingProto graphicalLoading={true} />
        }

        { /* --- NAVBAR ----------------------------------------------------*/ }
        { /*this.state.localView.navbar !== false*/true &&
          <View style={styles.navbar}>
            <Navbar isBlankStatusBar={ this.state.user !== undefined } />
          </View>
        }

        { this.state.user &&
          <View style={styles.content}>
            { modeView }
          </View>
        }
      </View>
    );
  }
}

AppComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

AppComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
