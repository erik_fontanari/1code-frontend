import PropTypes from 'prop-types';
import { INITIAL_APP_STATE } from '../../config/app';

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {
    ...INITIAL_APP_STATE,
    video: { width: undefined, height: undefined, duration: undefined },
      thumbnailUrl: undefined,
      videoUrl: undefined
  },
  PROP_TYPES : {
    loadingData  : PropTypes.func.isRequired,
    contentData  : PropTypes.shape(),
  },
  DEFAULT_PROPS : {}
};
