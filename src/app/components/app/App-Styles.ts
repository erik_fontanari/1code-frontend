import { StyleSheet } from 'react-native';
import { WIDTH_APP, HEIGHT_APP } from '../../config/app';

export const styles = StyleSheet.create({
  container: {
    width: WIDTH_APP,
    height: HEIGHT_APP,
    margin: 0,
    padding : 0,
    backgroundColor: 'white'
  },
  navbar: {
    width: WIDTH_APP,
    margin: 0,
    borderWidth: 1
  },
  content: {
    width: WIDTH_APP,
    height: HEIGHT_APP,
    margin: 0,
    padding : 0,
    marginBottom: 15,
    flex:1,
    top:0,
    zIndex:20,
    left:0
  }
});
