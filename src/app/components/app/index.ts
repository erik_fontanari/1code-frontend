import { connect } from 'react-redux';
import AppComponent from './App-Component';
import { getAllData } from '../../store/actions/appActions';

const mapStateToProps = ( state ) => {
  return {
    user : state.appData.user,
    localView : state.appData.localView,
    updateOn : ( new Date() ).getTime()
  }
};

const mapDispatchToProps = ( dispatch ) => ({
  loadingData : () => dispatch( getAllData( ) )
});

const App = connect( mapStateToProps, mapDispatchToProps )( AppComponent );

export default App
