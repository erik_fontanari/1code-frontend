import { Subject, Observable } from "rxjs";
import { filter, map } from 'rxjs/operators';
import { FORM_PUBLIC_FUNCTIONS, I__newForm, I__bodyFunction, I__overrideConfig, E__DynamicTypes } from './shared';

const getNewSettingsDefault = ( ) : I__newForm => {
  return {
    settings : {
      data : [], // qui verranno caricati i field del form
      design : {
        title : "", // titolo del form
        // inlineSubmit : false, // true -> allinea a dx il btn submit in versione grafica, altrimenti normale btn "clicca" in fondo
        printErrors : true, // true -> stampa gli errori di inserimento altrimenti no
        graphicalLoading : false, // true -> stampa uno spinner come caricamento, altrimenti testo "caricamento..."
        // printButtons : true, // true -> abilita le funzioni in addsOnFunctions
        addsOn : {
          // informations : {
          //   required : false // true -> abilita l'avviso dei campi required e nei campi required mette "*" prima del labelField del field
          // },
          functions : { // personalizzare le funzioni di seguito: se false non la darà disponibile, se true darà i valori di default, altrimenti si personallizzerà
            submit : true,
            reset : false,
            delete : false
          },
          otherFunctions : {}
        }
      }
    }
  } as I__newForm
}

// --- PUBLIC FUNCTIONS --------------------------------------------------------
export const createSettings = ( /*formName : string,*/ overrideConfig? : I__overrideConfig  ) : I__newForm => {
  let newForm = getNewSettingsDefault( /*formName*/ );

  // Override settings
  if( overrideConfig ) {

    // --- Write form field
    if( overrideConfig.data )
      newForm.settings.data = overrideConfig.data;

    // --- Override form design
    if( overrideConfig.design ) {
      let addsOn = {
        addsOn : {
          ...newForm.settings.design.addsOn,
          ...overrideConfig.design.addsOn
        }
      }
      newForm.settings.design = {
        ...newForm.settings.design,
        ...overrideConfig.design,
        ...addsOn
      };
    }
  }

  return newForm;
}

export const asyncFormManager = new Subject();

export const thisFormListener = ( formName : string ) : Observable<any> => {
  return asyncFormManager
    .pipe(
      filter( formContent => formContent.formName == formName ),
      map( thisForm => {
        let newContent = {
          ...thisForm
        }
        delete newContent.formName
        return newContent;
      })
    )
}

export const doFunctionOnForm = ( formName : string, formFunction : string, functionContent? : any) : void => {
  let body = {
    formName : formName,
    formFunction : formFunction
  };
  if( functionContent ) {
    body.functionContent = functionContent
  }
  asyncFormManager.next( body )
}
