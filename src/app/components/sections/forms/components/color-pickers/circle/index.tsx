// --- React Native materials --------------------------------------------------
import React, { Component } from 'react';
import {
  Text,
  View,
  Button,
  TouchableOpacity
} from 'react-native';
import { ColorPicker, toHsv, fromHsv } from 'react-native-color-picker';

// --- Environment materials ---------------------------------------------------
import { invertColor } from '../../../../../../models/colors';

export default class CircleColorPicker extends Component {

  constructor(props) {
    super(props)
    this.state = { color: this.props.starterColor }
  }

  sendDataAndClose = ( ) : void => {
    this.props.callback( this.getFromHsv() );
  }

  getFromHsv = ( ) : string => {
    return fromHsv( this.state.color )
  }

  render() {
    return (
      <View style={{}}>
        <ColorPicker
          //oldColor='purple'
          color={this.state.color}
          onColorChange={ color => this.setState({ color }) }
          onColorSelected={color => this.sendDataAndClose()}
          //onOldColorSelected={color => alert(`Old color selected: ${color}`)}
          style={{width:380,height:300}}
        />
        <TouchableOpacity
          style={{position:'absolute', top:'37%', left:'40%'}}
          onPress={()=>this.sendDataAndClose()}
        >
          <Text style={{fontSize:30,color: invertColor( this.getFromHsv() )}}>Fatto!</Text>
        </TouchableOpacity>
      </View>
    )
  }

}
