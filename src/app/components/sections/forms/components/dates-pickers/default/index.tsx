import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { /*Icon,*/ Button } from "native-base";
import DateTimePicker from '@react-native-community/datetimepicker';
import { getFormattedDate } from "../../../../../../models/dates-operations";
import { STATIC_ASSETS } from "../../../../../../config/app";
import { THEME_STYLES } from "../../../../../../theme/";

export default class DateDefaultPicker extends Component {

  state = {
    show         : false,
    mode         : "date",
    value        : this.props.val
  }

  constructor(props) {
    super(props);
  }

  render(){

    const onChange = (event, selectedDate) => {
      if( event.type === "dismissed" ) {
        this.setState({
          show : false
        })
      }
      if( event.type === "set" && selectedDate ) {
        this.setState({
          value        : selectedDate,
          show         : false
        })
        this.props.callback( selectedDate );
      }
    };

    return (
      <View>
        <View style={{paddingRight:0}}>
          <Button iconRight transparent onPress={()=>this.setState({
            show : true
          })}>
            <Text style={{ ...THEME_STYLES.APP.inputFormText, paddingLeft:0}}>{ getFormattedDate(this.state.value) }</Text>
            <STATIC_ASSETS.ARROW_DOWN width={27} height={22} />
          </Button>
        </View>
        {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={ this.state.value }
            mode={ this.state.mode }
            is24Hour={true}
            display="spinner"
            onChange={ onChange }
          />
        )}
      </View>
    );
  }
}
