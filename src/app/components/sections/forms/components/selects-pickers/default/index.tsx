import React, { Component } from 'react';
import { STATIC_ASSETS } from "../../../../../../config/app";
import { styles } from "./styles";

import RNPickerSelect from 'react-native-picker-select';

export default class DefaultSelectorPicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected2: `${this.props.selectedVal}`
    };
  }

  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
    this.props.callback( value );
  }

  render() {
    const printOptions = () : any[] => {
      return ( this.props.dataSet ).map( ( item ) => {
        return {
          label : item.label,
          value : item.value,
          key : item.value,
        }
      } )
    }

    return (
      <RNPickerSelect
        useNativeAndroidPickerStyle={false}
        placeholder={{
          label : this.props.placeholder,
          value : undefined
        }}
        style={styles}
        value={this.state.selected2}
        onValueChange={this.onValueChange2.bind(this)}
        Icon={() => <STATIC_ASSETS.ARROW_DOWN width={27} height={22} />}
        items={[...printOptions()]}
      />
    );
  }
}
