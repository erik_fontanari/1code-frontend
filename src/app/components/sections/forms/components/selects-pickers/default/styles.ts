
import { StyleSheet, View, Text } from 'react-native';
import { THEME_STYLES } from "../../../../../../theme/";

let basicSettings = {
  ...THEME_STYLES.APP.inputFormText
}

export const styles = StyleSheet.create({
	inputAndroid: {
    ...basicSettings
  },
	inputIOS: {
    ...basicSettings
  }
});
