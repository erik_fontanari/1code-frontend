// --- React Native materials --------------------------------------------------
import React, { Component } from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TextInput,
  Button,
  TouchableOpacity
} from 'react-native';
import { Icon, CheckBox } from 'react-native-elements';
import { Textarea, ListItem/*, CheckBox*/, Body, Container, Content } from 'native-base';

// --- Componet's needed -------------------------------------------------------
import { styles } from './Forms-Styles';
import { COMPONENT_SETTINGS } from './Forms-Settings';
import { thisFormListener, doFunctionOnForm, createSettings } from '../Forms-Manager';
import { checkContentType, I__newForm, printError, MAX_LEN, MIN_LEN, PASS_LABELS, thisFieldIsValid } from '../shared';
import CircleColorPicker from '../components/color-pickers/circle/';
import DefaultSelectorPicker from '../components/selects-pickers/default/';
import DateDefaultPicker from '../components/dates-pickers/default/';
import PassMeter from '../components/password/passmeter_check_security';

// --- Environment materials ---------------------------------------------------
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';
import { COLOR_PALETTES, STATIC_ASSETS } from '../../../../config/app';
import { getEntireDate, getTimeStampAtMidnight } from "../../../../models/dates-operations";

// --- Section Components ------------------------------------------------------
import LoadingProto from '../../loading/';
import ModalsProto from '../../modals/';

export default class FormsComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;
  subscription;

  constructor(props){
    super(props);
  }

  updateManager = ( partialState : any ) : void => {
    this.setState( partialState )
  }

  UNSAFE_componentWillReceiveProps( nextProps ) {
    if( JSON.stringify( nextProps.formSettingsBuffer.design.addsOn ) != JSON.stringify( this.state.design.addsOn ) ) {
      this.updateManager({
        design: nextProps.formSettingsBuffer.design
      });
    }
  }

  componentDidMount() {
    if( this.state.formNameLocal === "" && this.props.formName !== "") {
      this.subscription = thisFormListener( this.props.formName )
      .subscribe( formData => {

        // --- PARSING PUBLIC FUNCTIONS ------------------------------------------
        if( formData.formFunction ) {
          this.doFunction( formData.formFunction, formData.functionContent )
        }

      })

      if( this.props.formSettingsBuffer ) {
        doFunctionOnForm( this.props.formName, "form_init", this.props.formSettingsBuffer)
      }

      this.updateManager({
        formNameLocal: this.props.formName
      });
    }

  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  firstInitForm = ( functionContent : any ) : void => {
    let formSettings = createSettings( functionContent ) as I__newForm;
    this.initForm( formSettings.settings );
  }

  doFunction = ( formFunction : string, functionContent? : any ) : void => {

    // form_init function ------------------------------------------------------
    if( formFunction == "form_init" && functionContent ) {
      this.firstInitForm( functionContent );
    }

    // form_available function -------------------------------------------------
    if( formFunction == "form_available" ) {
      setTimeout( () => {
        this.updateManager({
          loading : false
        })
      }, 500)
    }

    // form_available loading -------------------------------------------------
    if( formFunction == "form_loading" ) {
      this.updateManager({
        loading : true
      })
    }

    // form_error function -----------------------------------------------------
    if( formFunction == "form_error" ) {

      // parsing possible errors

      // User credentials are wrong
      if( functionContent == "wrong_typing" ){
        this.updateManager({
          error : functionContent,
          loading : false
        })
      }
    }

    // form_reset function -----------------------------------------------------
    if( formFunction == "form_reset" ) {
      console.log("clear form")
      // this.initForm();
    }

  }

  isRequired = ( contentField : any ) : boolean => {
    let validation = ( contentField.validation !== undefined );
    return ( validation === true && contentField.validation.required === true );
  }

  needConfirm = ( contentField : any ) : boolean => {
    let validation = ( contentField.validation !== undefined );
    return ( validation === true && contentField.validation.confirm === true );
  }

  isTouched = ( fieldName : string ) : boolean => {
    return this.state.activityFields[ fieldName ] === "touched";
  }

  initForm = ( inputSettings : I__newForm ) : void => {
    if( inputSettings ) {
      let contentDataFields = {};
      let activityFields = {};
      ( inputSettings.data ).map( itemField => {

        // reformat values
        if( itemField.type === "default_date_picker" ){
          itemField.val = getTimeStampAtMidnight( itemField.val )
        }
        if( itemField.type === "password" ){
          itemField.validation = {
            ...itemField.validation,
            fieldIsHide : true
          }
        }

        // set untouched
        if( this.isRequired( itemField ) ) {
          activityFields[ itemField.id ] = "untouched";
        }

        contentDataFields[ itemField.id ] = itemField;

        // confirm this value
        if( this.needConfirm( itemField ) ) {
          let newField = `__confirm_this_${itemField.id}`;
          contentDataFields[ newField ] = {
            ...itemField,
            id : newField,
            labelField : `Conferma ${itemField.labelField}`,
            validation : {
              required : true,
              fieldIsHide : ( itemField.type === "password" )
            }
          }
          activityFields[ newField ] = "untouched";
        }

      });

      this.updateManager({
        contentDataFields : contentDataFields,
        design : inputSettings.design,
        activityFields : activityFields
      })
    }
  }

  changeContent = ( query : any, fieldName : string ) => {
    let contentDataFields = this.state.contentDataFields;
    let errorFields = this.state.errorFields;
    let activityFields = this.state.activityFields;

    contentDataFields[ fieldName ].val = query;
    activityFields[ fieldName ] = "touched";

    // required
    if( this.isRequired( contentDataFields[ fieldName ] ) && !query ) {
      errorFields[ fieldName ] = [ 'required' ];
    } else {
      if( errorFields[ fieldName ] ) {
        delete errorFields[ fieldName ]
      }

      // confirm field
      if( ( fieldName ).indexOf( "__confirm_this_" ) !== -1 ) {
        let findValues = fieldName.split( "__confirm_this_" );
        let correct = contentDataFields[ findValues[ 1 ] ].val == query;
        if( !correct ) {
          errorFields[ fieldName ] = [ 'confirm_wrong' ];
        }
      }

      // real field
      else {

        // this field needs confirm
        if( this.needConfirm( contentDataFields[ fieldName ] ) ) {
          let confirmField = `__confirm_this_${ fieldName }`;
          let correct = contentDataFields[ confirmField ].val == query;
          if( !correct ) {
            errorFields[ confirmField ] = [ 'confirm_wrong' ];
          }
        }

        // Check validity field
        if( thisFieldIsValid( contentDataFields[ fieldName ].type, query ) === false ) {
          errorFields[ fieldName ] = [ 'invalid' ]
        }
      }

    }

    this.updateManager({
      contentDataFields : contentDataFields,
      errorFields : errorFields
    })
  };

  getValueFromState = ( fieldName : string ) : any => this.state.contentDataFields[ fieldName ].val;

  __onSubmit = () : void => {

    let bufferState = {
      ...this.state,
      loading : true
    }
    delete bufferState.error;

    this.updateManager( bufferState )

    let submitValues = {};
    Object.values( this.state.contentDataFields ).map( itemField => {
      if( ( itemField.id ).indexOf( "__confirm_this_" ) == -1 ) {
        submitValues[ itemField.id ] = itemField.val;
      }
    });

    doFunctionOnForm( this.state.formNameLocal, "form_submit", submitValues );
  }

  getAddsOnInfo = ( inputElem : string, inputField : string, defaultVal : string  ) : any => {
    let res = defaultVal;
    if( this.state.design.addsOn && this.state.design.addsOn.functions && this.state.design.addsOn.functions[ inputElem ] ) {
      if( this.state.design.addsOn.functions[ inputElem ][ inputField ] ) {
        res = this.state.design.addsOn.functions[ inputElem ][ inputField ];
      }
    }
    return res;
  }

  getOtherDesignField = ( currentStyle : any ) : any => {
    let res = {
      width : '100%'
    };
    if( currentStyle ) {
      res = {
        ...res,
        ...currentStyle
      }
    }

    return res;
  }

  formIsAvailable = () : boolean => {
    let res = false;
    let objs = Object.values(
      this.state.activityFields
    );
    if( this.state.contentDataFields && objs.length > 0 ) {
      let errorField = ( Object.values( this.state.errorFields ).length > 0 ); // === false
      let untouchedFields = ( ( objs
      .filter( field => field === "untouched" ) ).length == 0 ) // === true
      res = ( errorField === false && untouchedFields === true )
    }
    return res;
  }

  printErrorField = ( itemID : string ) : string => {
    let res = "";
    let contentErrors = this.state.errorFields[ itemID ];

    if( contentErrors.indexOf( "required" ) !== -1 ) {
      res += " - Campo obbligatorio!"
    }
    if( contentErrors.indexOf( "confirm_wrong" ) !== -1 ) {
      res += " - Campo di conferma non corrispondente!"
    }
    if( contentErrors.indexOf( "invalid" ) !== -1 ) {
      res += " - Non valido!"
    }
    // questo flag è disattivato ed è solo per le password ( Too Short e Weak )
    // ma non serve visualizzare nulla a video, però se sono ( Too Short / Weak )
    // il form intero va in errore!
    // if( contentErrors.indexOf( "checkSecurity" ) !== -1 ) {
    //   res += " - checkSecurity!"
    // }

    return res
  }

  processCallBack = ( val : any ) : void => {
    if( val === "close_modal" ) {
      this.setState({
        modal : undefined
      })
    }
  }

  setSecurityPassword = ( itemID : string, isSafe : boolean ) : void => {
    let errorFields = this.state.errorFields[ itemID ];
    if( !errorFields ) {
      errorFields = [];
    }

    errorFields = errorFields.filter( item => item !== "checkSecurity" );
    if( !isSafe ) {
      errorFields.push( "checkSecurity" );
    }

    this.setState({
      errorFields : {
        ...this.state.errorFields,
        [ itemID ] : errorFields
      }
    })
  }

  render() {

    let titleForm;
    let listFields;
    if( this.state.design.title ) {
      titleForm = (
        <Text>{this.state.design.title}</Text>
      );
    }

    if( this.state.contentDataFields ) {
      listFields = Object.values( this.state.contentDataFields ).map( ( item, key ) => (
        <View style={{...styles.sectionStandAlone( this.getOtherDesignField( item.style ) ), minHeight: 72}} key={key}>

          { /* --- LABEL FIELD ---------------------------------------------*/ }
          { item.labelField &&
            <Text style={{width:'100%', margin:0, ...THEME_STYLES.APP.labelFieldFormText}}>
              { item.labelField }
            </Text>
          }

          { /* --- INPUT TEXT ----------------------------------------------*/ }
          { ( item.type === 'text' || item.type === 'number' ) &&
            <TextInput
              placeholder={ ( item.placeholder != "" && item.placeholder !== undefined && item.placeholder != null ) ? item.placeholder : '' }
              keyboardType = { item.type === 'number' ? "numeric" : "default"}
              // leftIcon={{ type: 'font-awesome', name: 'chevron-left' }}
              style={styles.inputStyle}
              onChangeText={value => this.changeContent( value, item.id ) }
              // errorStyle={{ color: 'red' }}numeric
              // errorMessage='ENTER A VALID ERROR HERE'
              // ref={this.input}
              value={`${this.getValueFromState( item.id )}`}
              // minLength={2}
            />
          }

          { /* --- INPUT EMAIL ----------------------------------------------*/ }
          { item.type === 'email' &&
            <TextInput
              placeholder={ item.placeholder == true ? 'Es: nome.cognome@email.com' : '' }
              style={styles.inputStyle}
              onChangeText={value => this.changeContent( value, item.id ) }
              value={`${this.getValueFromState( item.id )}`}
            />
          }

          { /* --- INPUT PASSWWORD -----------------------------------------*/ }
          { item.type === 'password' &&
            <SafeAreaView style={styles.container}>
              <View style={{flexDirection:'row',borderBottomWidth:1,borderColor: '#CCCCCC'}}>
                <TextInput
                  style={{...styles.inputStyle,width:'90%',borderBottomWidth:0}}
                  onChangeText={value => this.changeContent( value, item.id ) }
                  value={`${this.getValueFromState( item.id )}`}
                  maxLength={50}
                  secureTextEntry={ this.state.contentDataFields[ item.id ].validation.fieldIsHide }
                />

                <TouchableOpacity onPress={ () => {
                  let thisField = this.state.contentDataFields[ item.id ];
                  thisField.validation.fieldIsHide = !thisField.validation.fieldIsHide;
                  this.setState({
                    contentDataFields : {
                      ...this.state.contentDataFields,
                      [ item.id ] : thisField
                    }
                  })
                }}>

                  {
                    this.state.contentDataFields[ item.id ].validation.fieldIsHide ?
                    <STATIC_ASSETS.EYE_OPEN
                      width={27}
                      height={27}
                    /> :
                    <STATIC_ASSETS.EYE_CLOSE
                      width={27}
                      height={27}
                    />
                  }

                </TouchableOpacity>


                {/*

                  <Icon
                  name={ this.state.contentDataFields[ item.id ].validation.fieldIsHide ? 'eye' : 'eye-slash' }
                  type='font-awesome'
                  color={ COLOR_PALETTES.lightblue }
                  size={27}
                  onPress={ () => {
                  let thisField = this.state.contentDataFields[ item.id ];
                  thisField.validation.fieldIsHide = !thisField.validation.fieldIsHide;
                  this.setState({
                  contentDataFields : {
                  ...this.state.contentDataFields,
                  [ item.id ] : thisField
                }
              })
            }}
            />
                  */}
              </View>
              { ( item.validation && item.validation.checkSecurity === true && ( this.isRequired( this.state.contentDataFields[ item.id ] ) && this.isTouched( item.id ) )) &&
                <View style={{}}>
                  <PassMeter
                    showLabels
                    password={ this.getValueFromState( item.id ) }
                    maxLength={ MAX_LEN }
                    minLength={ MIN_LEN }
                    labels={ PASS_LABELS }
                    callback={ ( isPasswordSafe : boolean ) => this.setSecurityPassword( item.id, isPasswordSafe ) }
                  />
                </View>
              }
            </SafeAreaView>
          }

          { /* --- INPUT CHECKBOX ------------------------------------------*/ }
          { item.type === 'checkbox' &&
            <View style={{margin:0,marginLeft:-20,marginTop:-15,padding:0}}>
              <CheckBox
                uncheckedIcon={ <STATIC_ASSETS.CHECKBOX_OFF width={22} height={22} /> }
                checkedIcon={ <STATIC_ASSETS.CHECKBOX_ON width={22} height={22} /> }
                containerStyle={[{borderWidth:0,backgroundColor:'transparent',margin:0}]}
                checked={ this.getValueFromState( item.id ) }
                onPress={ () => this.changeContent( !this.getValueFromState( item.id ), item.id ) }
                title={
                  <Text style={{marginLeft:7,margin:0,paddingBottom:10,borderBottomWidth:1,borderColor: '#CCCCCC'}}>
                    { item.content() }
                  </Text>
                }
              />

            </View>
          }

          { /* --- TEXTAREA ------------------------------------------------*/ }
          { item.type === 'textarea' &&
            <Textarea
              // style={styles.inputStyle}
              onChangeText={value => this.changeContent( value, item.id ) }
              value={`${this.getValueFromState( item.id )}`}
              bordered
              rowSpan={ 5 }
            />
          }

          { /* --- COLOR PICKER --------------------------------------------*/ }
          { item.type === 'color_picker' &&
            <Button
              style={styles.inputStyle}
              onPress={()=>this.updateManager({
                modal : {
                  view : item.type,
                  id : item.id,
                  title : 'Scegli colore',
                  closeButton : true
                }
              })}
              title=""
              color={`${this.getValueFromState( item.id )}`}
            />
          }

          { /* --- DEFAULT SELECT PICKER -----------------------------------*/ }
          { item.type === 'default_select_picker' &&
            <View style={{...styles.inputStyle,marginLeft:-5}}>
              <DefaultSelectorPicker
                dataSet={ item.dataSet }
                selectedVal={item.val}
                placeholder={item.placeholder}
                callback={ value => this.changeContent( value, item.id ) }
              />
            </View>
          }

          { /* --- DEFAULT DATE PICKER -------------------------------------*/ }
          { item.type === 'default_date_picker' &&
            <View style={{...styles.inputStyle}}>
              <DateDefaultPicker
                val={ item.val }
                callback={value => this.changeContent( value, item.id ) }
              />
            </View>
          }

          { /* --- ERRORS FIELD --------------------------------------------*/ }
          { this.state.errorFields[ item.id ] &&
            <Text style={{width:'100%', margin:0, fontSize:12,color:'red'}}>
              { this.printErrorField( item.id ) }
            </Text>
          }

        </View>
      ))
    }

    const getDataFromModalAndClose = ( value : string, itemID : string ) : void => {
      this.changeContent( value, itemID );

      let newState = {
        ...this.state,
        modal : undefined
      }
      this.updateManager( newState )
    }

    const getModalView = ( item ) => (
      <View style={{}}>

        { item.view === "color_picker" &&
          <CircleColorPicker
            starterColor={`${this.getValueFromState( item.id )}`}
            callback={ ( value ) => getDataFromModalAndClose( value, item.id ) } />
        }

      </View>
    )

    let otherFunctions;
    if( this.state.design.addsOn && this.state.design.addsOn.otherFunctions && this.state.loading === false ) {
      otherFunctions =Object.keys( this.state.design.addsOn.otherFunctions ).map( item => (
        <THEME_COMPONENTS.BUTTON
          key={ item }
          onPress={()=>doFunctionOnForm( this.state.formNameLocal, `form_${ item }` )}
          label={ this.state.design.addsOn.otherFunctions[ item ].label }
          designType={ this.state.design.addsOn.otherFunctions[ item ].designType }
        />
      ));
    }

    return (
      <View style={{margin:0,padding:0,flex:1/*,width:'100%',height:'100%',flex:1*/}}>

        { /* --- Modal Setup -----------------------------------------------*/ }
        { this.state.modal &&
          <ModalsProto
            contentView={()=>getModalView( this.state.modal )}
            settings={{
              closeButton : this.state.modal.closeButton,
              title       : this.state.modal.title,
            }}
            callback={ ( val ) => this.processCallBack( val ) }
          />
        }

        <View style={{
          width:'100%',
          height:'100%',
          // flexDirection: 'row',
          // flexWrap:'wrap',
          justifyContent:'space-between',
          flex:1
          // alignItems: 'stretch',
          // flex:1
        }}>
          <View style={{flex:0.8}}>

            { /* --- Title form --------------------------------------------*/ }
            { titleForm }

            { /* --- List field available ----------------------------------*/ }
            { listFields }

            { /* --- Errors form -------------------------------------------*/ }
            { this.state.error &&
              <View style={styles.errorContent}>
                <Text style={styles.errorText}>
                  { printError( this.state.error ) }
                </Text>
              </View>
            }

          </View>

          <View style={{justifyContent:'flex-end',flex:0.2,paddingBottom:10}}>

            { /* --- Other button functions ---------------------------------*/}
            { otherFunctions }

            { /* --- Submit button -----------------------------------------*/ }
            <View style={styles.buttonStandAlone( this.getAddsOnInfo( "submit", "style", { width: '100%',padding:0,margin:0,justifyContent:'center',marginTop:-15 } ) )}>
              { this.state.loading === false &&
                <THEME_COMPONENTS.BUTTON
                  onPress={()=>this.__onSubmit()}
                  label={ this.getAddsOnInfo( "submit", "title", "Salva" ) }
                  designType={ !this.formIsAvailable() == false ? 'main' : 'disabled' }
                />
              }
              { this.state.loading === true &&
                <View style={{height:30,alignItems:'center'}}>
                  <LoadingProto graphicalLoading={this.state.design.graphicalLoading} />
                </View>
              }
            </View>

            { /* --- Reset button ------------------------------------------*/ }
            { ( this.state.loading === false && this.state.design.addsOn && this.state.design.addsOn.functions && this.state.design.addsOn.functions.reset ) &&
              <View style={styles.buttonStandAlone( this.getAddsOnInfo( "reset", "style", styles.buttonIconOnRow ) )}>
                <TouchableOpacity
                  style={{}}
                  onPress={()=>doFunctionOnForm( this.state.formNameLocal, "form_reset" )}
                >
                  <Icon
                    name='refresh'
                    type='font-awesome'
                    color='#f50'
                  />
               </TouchableOpacity>
              </View>
            }

            { /* --- Delete form button ------------------------------------*/ }
            { ( this.state.loading === false && this.state.design.addsOn && this.state.design.addsOn.functions && this.state.design.addsOn.functions.delete ) &&
              <View style={styles.buttonStandAlone( this.getAddsOnInfo( "delete", "style", styles.buttonIconOnRow ) )}>
                <TouchableOpacity
                  style={{}}
                  onPress={()=>doFunctionOnForm( this.state.formNameLocal, "form_delete" )}
                >
                  <Icon
                    name='trash'
                    type='font-awesome'
                    color='#f50'
                  />
                </TouchableOpacity>
              </View>
            }

          </View>

        </View>

      </View>
    );
  }

}

FormsComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

FormsComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
