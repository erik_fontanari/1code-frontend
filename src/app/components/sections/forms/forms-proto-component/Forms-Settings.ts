import PropTypes from 'prop-types';
import { I__designForm } from '../shared';

interface I__FormsState {
  formNameLocal : string;
  contentDataFields : any;
  design : I__designForm;
  loading : boolean;
  error?: string;
  modalData? : any;
  errorFields : any;
  activityFields : any;
}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {
    formNameLocal : "",
    contentDataFields : {},
    design : {},
    loading : false,
    errorFields : {},
    activityFields : {}
  } as I__FormsState,
  PROP_TYPES : {
    formName           : PropTypes.string,
    formSettingsBuffer : PropTypes.shape()
  },
  DEFAULT_PROPS : {}
};
