import { StyleSheet, Dimensions } from 'react-native';
import { WIDTH_APP, HEIGHT_APP } from '../../../../config/app';
import { THEME_STYLES } from '../../../../theme/';

export const styles = StyleSheet.create({
  formContainer: {
  },
  titleForm:{
    fontSize : 20,
    justifyContent: 'center',
    width:'100%',
    textAlign: 'center',
    fontWeight:'bold',
    marginTop:10,
    marginBottom:10
  },
  buttonsRow : {
  },
  buttonIconOnRow : {
    alignItems:'flex-end',
    width:'15%'
  },
  inputStyle : {
    ...THEME_STYLES.APP.inputFormText,
    borderColor: '#CCCCCC',
    borderTopWidth: 0,
    borderBottomWidth: 1
  },
  sectionStandAlone: ( externalSettings ) => {
    return {
      padding:10,
      paddingLeft:0,
      paddingRight:0,
      ...externalSettings
    }
  },
  buttonStandAlone: ( externalSettings ) => {
    return {
      height:60,
      padding:10,
      paddingTop:20,
      paddingBottom:0,
      ...externalSettings
    }
  },
  errorContent:{
    marginTop:15,
    width:'100%'
  },
  errorText:{
    color:'red',
    fontWeight:'bold'
  }
});
