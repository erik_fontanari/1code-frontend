// --- FORMS - INTERFACES ---
// Forms settings
interface I__ItemFormConfig {
  validators  : any[];
  type? : string;
  rows? : number;
}

interface I__dataSet {
  id : number,
  label : string
}

interface I__dataForm {
  id : string; // id univoco per il field
  type : string; // compreso in Object.keys( FORM_TYPE_FIELDS )
  labelField? : string; // titolo label del field corrente
  val? : any[] | string | number,
  dataSet? : I__dataSet[], // solo se type == "defaultSelect" | "checkboxes_In_Select" | "async_suggestion_Select"
  min? : Date | number, // solo se type == "defaultSlider" | "defaultNumber" | "defaultDateTimePicker"
  max? : Date | number, // solo se type == "defaultSlider" | "defaultNumber" | "defaultDateTimePicker"
  step? : number, // solo se type == "defaultSlider"

  // solo se type == "defaultFile"
  mode? : {
    style : string,
    type: string,
    multiple : boolean
  }
}

interface I__addsOnFunction {
  label : string
}

interface I__addsOn {
  // informations? : {
  //   required : boolean
  // },
  functions? : {
    submit? : ( false | true | I__addsOnFunction ),
    reset? :  ( false | I__addsOnFunction )
  },
  otherFunctions? : any
}

interface I__designForm {
  title? : string,
  // inlineSubmit? : boolean,
  printErrors? : boolean,
  graphicalLoading? : boolean,
  // printButtons? : boolean,
  addsOn? : I__addsOn
}

interface I__dynamicsForm {
  available? : boolean,
  reloadAfterSubmit? : boolean
}

interface IFormMappersField {
  form_mappers : string,
  functions_mappers : string
}

interface IFormMappers {
  service : IFormMappersField,
  component : IFormMappersField,
}

export interface I__bodyFunction {
  name : string;
  param? : any;
  mappers : IFormMappersField;
}

export interface I__newForm {
  emitData : any,
  dynamics : I__dynamicsForm,
  settings : {
    data : I__dataForm[],
    design : I__designForm
  },
  additionalTemplates? : any,
  mappers : IFormMappersField
}

export interface I__overrideConfig {
  data? : I__dataForm[],
  design? : I__designForm,
  dynamics? : I__dynamicsForm,
  additionalTemplates? : any
}
// --- FORMS - INTERFACES ---

// --- FORMS - CONSTS ---
const FORM_TYPE_PATTERNS = {
  text : /(?=^.{3,}$)(?=.*^[^0-9\[\]:#@;\]\\[\\{\\}\|=\+\*\?<>\/\\]+$).*$/,
  email : /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  // message : /(?=^.{1,}$).*$/,
  password : /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
  number : /^(-?[1-9]+\\d*([.]\\d+)?)$|^(-?0[.]\\d*[1-9]+)$|^0$/
}

// export const checkContentType = ( inputVal : any, inputType : string ) : boolean => {
//   let reg = FORM_TYPE_PATTERNS[ inputType ];
//   console.log( inputVal, reg, reg.test( inputVal ), typeof(inputVal) )
//   return reg.test( inputVal )
//   // return Regex.Match(
//   //   inputVal,
//   //   FORM_TYPE_PATTERNS[ inputType ]
//   //   // RegexOptions.IgnoreCase
//   // ).Success;
// }
export const ERRORS_CONFIG = {
  NO : null,
  NOT_EQUAL : {
    NotEqual: true
  }
}

export const FORM_PUBLIC_FUNCTIONS = {
  RESET : {
    name : 'reset'
  },
  FORM_AVAILABLE : {
    name : 'form_available'
  }
}
// --- FORMS - CONSTS ---

// --- FORMS - ENUMS ---
export enum E__DynamicTypes {
  CONFIG = 'config',
  FUNCTION = 'function'
}
// --- FORMS - ENUMS ---
const static_contents = {
  errors : {
    wrong_typing : "- Email o Nickname già presenti. Riprova!"
  }
}

export const printError = ( errorName : string ) : string => {
  return static_contents.errors[ errorName ];
}

export const MAX_LEN = 15,
 MIN_LEN = 6,
 PASS_LABELS = ["Too Short", "Weak", "Normal", "Strong", "Secure"];

export const thisFieldIsValid = ( inputTypeField : string, queryVal : any ) : boolean => {
  let res = true;

  // email validation
  if( inputTypeField === "email" || inputTypeField === "password" ) {
    let reg = FORM_TYPE_PATTERNS[ inputTypeField ];
    res = reg.test( queryVal );
  }

  return res;
}
