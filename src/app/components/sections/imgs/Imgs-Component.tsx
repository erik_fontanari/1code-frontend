// --- React Native materials --------------------------------------------------
import React, { Component } from 'react';
import { View, Image } from 'react-native';

// --- Componet's needed -------------------------------------------------------
import { styles } from './Imgs-Styles';
import { COMPONENT_SETTINGS } from './Imgs-Settings';

export default class ImgsComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  render() {

    return (
      <View style={styles.imgContainer}>
        <Image
          source={ this.props.thisImg }
          style={ styles[ this.props.imgType ] }
        />
      </View>
    );
  }
}

ImgsComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

ImgsComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
