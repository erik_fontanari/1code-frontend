import PropTypes from 'prop-types';

interface I__ImgsState {}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {} as I__ImgsState,
  PROP_TYPES : {
    imgType : PropTypes.string,
    thisImg : PropTypes.any
  },
  DEFAULT_PROPS : {}
};
