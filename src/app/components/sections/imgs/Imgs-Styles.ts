import { StyleSheet, Dimensions } from 'react-native';
// import { WIDTH_APP } from '../../../config/app';

let basicIMG = {
  resizeMode: 'contain',
  flex: 1,
  aspectRatio: 2.2, // Your aspect ratio
}

export const styles = StyleSheet.create({
  imgContainer: {
    flexDirection: 'row'
  },
  logo : {
    ...basicIMG,
  },
  topScreenView : {
    height:100,
    width:175,
    resizeMode: 'contain',
    flex:1,
    marginTop:27,
    marginBottom:33
  },
  avatar : {
    ...basicIMG,
    borderRadius:70
  }
});
