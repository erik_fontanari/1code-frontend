import React, { Component } from 'react';
import {
  Text,
  View, SafeAreaView
} from 'react-native';
import { styles } from './Loading-Styles';
import { COMPONENT_SETTINGS } from './Loading-Settings';
import Imgs from '../imgs/';
import { STATIC_ASSETS } from '../../../config/app';

export default class LoadingComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  render() {

    return (
      <SafeAreaView style={styles.container}>
        { this.props.graphicalLoading == true ?
          <View style={styles.first_loading}>
            <View style={styles.logo}>
              <View style={{justifyContent:'center',flexDirection:'row'}}>
                <STATIC_ASSETS.LOGO_APP height={100} width={175} />
              </View>

              {/*
                <Imgs imgType="logo" thisImg={ STATIC_ASSETS.LOGO_APP } />

                */}

            </View>
          </View> :
          <Text style={styles.textSettings}>Caricamento in corso...</Text>
        }
      </SafeAreaView>
    );
  }

}

LoadingComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

LoadingComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
