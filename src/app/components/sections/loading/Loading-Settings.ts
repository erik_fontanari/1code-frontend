import PropTypes from 'prop-types';

interface I__LoadingState {}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {} as I__LoadingState,
  PROP_TYPES : {
    graphicalLoading : PropTypes.bool
  },
  DEFAULT_PROPS : {}
};
