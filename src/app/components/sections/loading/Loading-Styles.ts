import { StyleSheet, Dimensions } from 'react-native';
// import Constants from 'expo-constants';
import { THEME_STYLES } from '../../../theme/';

export const styles = StyleSheet.create({
  container: {
    ...THEME_STYLES.APP.container
  },
  first_loading: {
    ...THEME_STYLES.APP.mainContent
  },
  logo : {
    width: '50%'
  },
  textSettings : {
    ...THEME_STYLES.APP.mainText,
    textAlign: 'center'
  }
});
