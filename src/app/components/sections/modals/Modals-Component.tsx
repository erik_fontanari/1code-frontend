import React, { Component } from 'react';
import {
  Alert,
  Modal,
  TouchableHighlight,
  View
} from "react-native";
import { Container, Header, Content, Card, CardItem, Body, Text } from "native-base";
import { styles } from './Modals-Styles';
import { COMPONENT_SETTINGS } from './Modals-Settings';

export default class ModalsComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  setModalVisible = ( visible ) => {
    let settingsModal = {
      modalVisible: visible
    };

    this.setState( settingsModal );
    this.props.modalOnRedux( settingsModal );
  }

  componentDidMount() {
    this.setModalVisible( true );
  }

  closeModal = () : void => {
    this.setModalVisible( !this.state.modalVisible );
    this.props.callback( "close_modal" )
  }

  render() {
    const { modalVisible } = this.state;

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          // style={{height:100}}
          onRequestClose={() => {
            console.log( "Modal has been closed." )
          }}
        >
          <Container style={styles.centeredView}>
            <View style={styles.modalView}>
              <Card transparent>

                { /* --- Header ------------------------------------------- */ }
                <CardItem header bordered>
                  <View style={styles.modalHeader}>

                    { /* --- Title Modal ---------------------------------- */ }
                    <Text style={styles.modalTitle}>
                      { this.props.settings.title }
                    </Text>

                    { /* --- Closing button modal ------------------------- */ }
                    { this.props.settings.closeButton == true &&
                      <TouchableHighlight
                        style={styles.closeModalButton}
                        onPress={() => {
                          this.closeModal();
                        }}
                      >
                        <Text style={styles.closeModalButtonText}>X</Text>
                      </TouchableHighlight>
                    }
                  </View>
                </CardItem>

                { /* --- Content Body Scrollable -------------------------- */ }
                <Content>
                  <CardItem>

                    { /* --- Content view --- */ }
                    <View style={styles.modalContent}>
                      { this.props.contentView() }
                    </View>

                  </CardItem>
                </Content>

              </Card>
            </View>
          </Container>
        </Modal>
      </View>
    );
  }

}

ModalsComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

ModalsComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
