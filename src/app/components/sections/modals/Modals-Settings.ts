import PropTypes from 'prop-types';

interface I__ModalsState {
  modalVisible : boolean;
}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {
    modalVisible: false
  } as I__ModalsState,
  PROP_TYPES : {
    contentView : PropTypes.func.isRequired,
    modalOnRedux : PropTypes.func.isRequired,
    callback : PropTypes.func,
    settings : PropTypes.shape({
      closeButton : PropTypes.boolean
    })
  },
  DEFAULT_PROPS : {}
};
