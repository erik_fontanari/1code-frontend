import { StyleSheet, Dimensions } from 'react-native';
import { WIDTH_APP } from '../../../config/app';

export const styles = StyleSheet.create({
  centeredView: {
    backgroundColor : 'rgba(0,0,0,0.7)'
  },
  modalView: {
    margin: 20,
    padding:0,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalHeader : {
    flexDirection:'row',
    justifyContent:'space-between',
    margin:0,
    width:'100%'
  },
  modalContent : {
    flexDirection:'row',
    justifyContent:'center',
    flexWrap:'wrap'
  },
  modalTitle : {

  },
  closeModalButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    elevation: 2,
    alignSelf: 'flex-end'
  },
  closeModalButtonText: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  // modalText: {
  //   marginBottom: 15,
  //   textAlign: "center"
  // }
});
