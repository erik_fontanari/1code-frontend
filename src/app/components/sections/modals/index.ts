import { connect } from 'react-redux';
import ModalsComponent from './Modals-Component';
import { settingsModalConfig } from '../../../store/actions/appActions';

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => ({
  modalOnRedux: ( settingsModal : any ) => dispatch(  settingsModalConfig( settingsModal ) ),
});

const ModalsProto = connect( mapStateToProps, mapDispatchToProps )( ModalsComponent );

export default ModalsProto;
