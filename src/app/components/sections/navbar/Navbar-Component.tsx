import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';

import { styles } from './Navbar-Styles';
import { COMPONENT_SETTINGS } from './Navbar-Settings';
import { STATIC_ASSETS } from '../../../config/app';

export default class NavbarComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  // Questa bisogna lasciarla ( anche vuota ) per il refresh automatico!
  UNSAFE_componentWillReceiveProps(nextProps){}

  goBack = () : void => {
    if( this.props.localView.step === 1 ) {
      this.props.updateLocalView( "INIT" )
    } else {
      this.props.updateLocalView( this.props.localView.voice, "BACK" );
    }
  }

  render() {
    let title = "";
    if( this.props.localView.name === "registration" || this.props.localView.name === "access" ){
      if( typeof( this.props.localView.navbar.titles ) === "string" ) {
        title = this.props.localView.navbar.titles
      }
      if( this.props.localView.navbar.titles instanceof Array ) {
        title = this.props.localView.navbar.titles[ this.props.localView.step - 1 ]
      }
    }

    const ComponentLeft = () => {

      let img;

      if( title ) {
        img = (
          <STATIC_ASSETS.ARROW_BACK height={20} />
        )
      }

      return(
        <View style={{ flex: 1, alignItems: 'flex-start'}} >
          <TouchableOpacity style={ {justifyContent:'center', flexDirection: 'row',paddingLeft:20}} onPress={() => this.goBack()}>
            { img }
          </TouchableOpacity>
        </View>
      );
    };

    const ComponentCenter = () => {
      return(
        <View style={{ flex: 1, alignItems:'center' }}>
          <Text style={{ ...styles.title,color: 'black' }}>
            { title }
          </Text>
        </View>
      );
    };

    const ComponentRight = () => {
      return(
        <View style={{ flex: 1, alignItems: 'flex-end', }}>
        </View>
      );
    };

    return (
      <View style={ styles.container }>
        <NavigationBar
          componentLeft     = { () =>  <ComponentLeft />   }
          componentCenter   = { () =>  <ComponentCenter /> }
          componentRight    = { () =>  <ComponentRight />  }
          navigationBarStyle= { styles.navigationBarStyle( this.props.isBlankStatusBar ) }
          statusBarStyle    = { styles.statusBarStyle( this.props.isBlankStatusBar ) }
        />
      </View>
    );
  }
}

NavbarComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

NavbarComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
