import PropTypes from 'prop-types';

interface I__NavbarState {}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {} as I__NavbarState,
  PROP_TYPES : {
    localView : PropTypes.shape(),
    updateOn: PropTypes.number,
    updateLocalView  : PropTypes.func.isRequired,
    isBlankStatusBar : PropTypes.bool
  },
  DEFAULT_PROPS : {}
};
