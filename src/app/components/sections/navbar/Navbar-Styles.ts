import { StyleSheet } from 'react-native';
import { PLATFORM_APP } from '../../../config/app';
import { THEME_STYLES } from '../../../theme/';

export const styles = StyleSheet.create({
  container : {
    top:-1,
    left:-1,
    marginRight:-2,
    height: ( PLATFORM_APP.OS === 'ios' ? 43 : 54 )
  },
  title: {
    ...THEME_STYLES.APP.darkText,
  },
  navigationBarStyle : ( isBlankStatusBar : boolean ) => {
    return {
      ...( isBlankStatusBar ? THEME_STYLES.APP.lightContent : THEME_STYLES.APP.blueContent )
    }
  },
  statusBarStyle : ( isBlankStatusBar : boolean ) => {
    return {
      ...( isBlankStatusBar ? THEME_STYLES.APP.lightContent : THEME_STYLES.APP.blueContent ),
      barStyle: `${ ( isBlankStatusBar ? 'dark' : 'light' ) }-content`
    }
  }
});
