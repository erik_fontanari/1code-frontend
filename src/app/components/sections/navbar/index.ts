import { connect } from 'react-redux';
import NavbarComponent from './Navbar-Component';
import { updateLocalView_UnLogged } from '../../../store/actions/appActions';

const mapStateToProps = (state) => {
  return {
    localView : state.appData.localView
  }
};

const mapDispatchToProps = (dispatch) => ({
  updateLocalView: ( newView : string, requestType? : string ) => dispatch( updateLocalView_UnLogged( newView, requestType ) ),
});

const Navbar = connect( mapStateToProps, mapDispatchToProps )( NavbarComponent );

export default Navbar;
