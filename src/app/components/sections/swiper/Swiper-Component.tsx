// --- React Native materials --------------------------------------------------
import React, { Component } from 'react';
import { View } from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';

// --- Componet's needed -------------------------------------------------------
import { styles } from './Swiper-Styles';
import { COMPONENT_SETTINGS } from './Swiper-Settings';

import { COLOR_PALETTES } from '../../../config/app';

export default class SwiperComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  render() {

    return (
      <View style={styles.container}>
        <SwiperFlatList
          autoplay
          // autoplayDelay={2}
          autoplayLoop
          // index={2}
          showPagination
          paginationStyle={styles.paginationStyle}
          paginationStyleItem={styles.paginationActiveColor}
          paginationActiveColor={ COLOR_PALETTES.lightblue }
          style={styles.swiperStyles}
          data={ this.props.dataSwiper }
          renderItem={ ({ item }) => (
            <View style={{ ...styles.child}}>
              { this.props.renderItemComponent( item ) }
            </View>
          )}
        >
        </SwiperFlatList>
      </View>
    );
  }
}

SwiperComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

SwiperComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
