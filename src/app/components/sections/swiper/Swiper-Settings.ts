import PropTypes from 'prop-types';

interface I__SwiperState {}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {} as I__SwiperState,
  PROP_TYPES : {
    dataSwiper : PropTypes.array.isRequired,
    renderItemComponent : PropTypes.func.isRequired
  },
  DEFAULT_PROPS : {}
};
