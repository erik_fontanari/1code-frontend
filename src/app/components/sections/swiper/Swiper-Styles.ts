import { StyleSheet, Dimensions } from 'react-native';
// import { WIDTH_APP } from '../../../config/app';
import { THEME_STYLES } from '../../../theme/';

export const styles = StyleSheet.create({
  container : {
    height: '100%',
    width: '100%',
    alignItems:'center',
    flex:1
  },
  paginationStyle : {
    position:'relative',
    width:'100%',top:-30
  },
  paginationActiveColor : {
    width:6,
    height:6
  },
  swiperStyles : {
    flex:1,
    flexWrap:'wrap',
    flexDirection:'column',
    width:'100%',
    height:'100%'
  },
  child: {
    width: '100%',
    height: '100%',
  }
});
