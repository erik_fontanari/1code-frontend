// --- React Native materials --------------------------------------------------
import React, { Component } from 'react';
import { View } from 'react-native';
// import { MaterialIcons, Octicons } from '@expo/vector-icons';
// import { Video } from 'expo';
import Video from 'react-native-video';

// --- Componet's needed -------------------------------------------------------
import { styles } from './VideoProto-Styles';
import { COMPONENT_SETTINGS } from './VideoProto-Settings';

export default class VideoProtoComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;
  player;
  onBuffer;
  videoError;

  constructor(props){
    super(props);
  }

  // handlePlayAndPause = () => {
  //   this.setState((prevState) => ({
  //      shouldPlay: !prevState.shouldPlay
  //   }));
  // }
  //
  // handleVolume = () => {
  //   this.setState(prevState => ({
  //     mute: !prevState.mute,
  //   }));
  // }

  render() {

    return (
      <View style={styles.container}>

        <Video source={{uri: this.props.urlVideo}}   // Can be a URL or a local file.
          // ref={(ref) => {
          //  this.player = ref
          // }}                                      // Store reference
          onBuffer={this.onBuffer}                // Callback when remote video is buffering
          onError={this.videoError}               // Callback when video cannot be loaded
          style={styles.backgroundVideo}
        />

      </View>
    );
  }
}

VideoProtoComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

VideoProtoComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;


/*



        <Video
          source={{ uri: this.props.urlVideo }}
          shouldPlay={this.state.shouldPlay}
          resizeMode="cover"
          style={styles.videoContainer}
          isMuted={this.state.mute}
        />
        <View style={styles.controlBar}>
          <MaterialIcons
            name={this.state.mute ? "volume-mute" : "volume-up"}
            size={45}
            color="white"
            onPress={this.handleVolume}
          />
          <MaterialIcons
            name={this.state.shouldPlay ? "pause" : "play-arrow"}
            size={45}
            color="white"
            onPress={this.handlePlayAndPause}
          />
        </View>









        <Video
          source={{ uri: this.props.urlVideo }}
          shouldPlay={this.state.shouldPlay}
          resizeMode="cover"
          style={styles.videoContainer}
          isMuted={this.state.mute}
        />
        <View style={styles.controlBar}>
          <MaterialIcons
            name={this.state.mute ? "volume-mute" : "volume-up"}
            size={45}
            color="white"
            onPress={this.handleVolume}
          />
          <MaterialIcons
            name={this.state.shouldPlay ? "pause" : "play-arrow"}
            size={45}
            color="white"
            onPress={this.handlePlayAndPause}
          />
        </View>


*/
