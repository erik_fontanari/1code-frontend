import PropTypes from 'prop-types';

interface I__VideoProtoState {
  mute : boolean;
  shouldPlay : boolean;
}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {
    mute: false,
    shouldPlay: true,
  } as I__VideoProtoState,
  PROP_TYPES : {
    urlVideo : PropTypes.string.isRequired
  },
  DEFAULT_PROPS : {}
};
