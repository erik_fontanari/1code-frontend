import { connect } from 'react-redux';
import VideoProtoComponent from './VideoProto-Component';

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => ({});

const VideoCustom = connect( mapStateToProps, mapDispatchToProps )( VideoProtoComponent );

export default VideoCustom;
