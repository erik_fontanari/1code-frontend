import React, { Component } from 'react';
import {
  Text,Image,
  View,
} from 'react-native';
import Swiper from 'react-native-swiper'

import { styles } from './HomeLogged-Styles';
import { COMPONENT_SETTINGS } from './HomeLogged-Settings';

import Imgs from '../../../sections/imgs/';
import { STATIC_ASSETS } from '../../../../config/app';
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';

export default class HomeLoggedComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={ THEME_STYLES.APP.importantText }>Home Logged</Text>
      </View>
    );
  }
}

HomeLoggedComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

HomeLoggedComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
