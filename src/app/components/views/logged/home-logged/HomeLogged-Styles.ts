import { StyleSheet, Dimensions } from 'react-native';
import { THEME_STYLES } from '../../../../theme/';
import { WIDTH_APP, HEIGHT_APP, COLOR_PALETTES } from '../../../../config/app';

export const styles = StyleSheet.create({
  container: {
    flex: 0,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop:40,
  }
});
