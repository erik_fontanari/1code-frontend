import { connect } from 'react-redux';
import HomeLoggedComponent from './HomeLogged-Component';

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => ({});

const HomeLogged = connect( mapStateToProps, mapDispatchToProps )( HomeLoggedComponent );

export default HomeLogged;
