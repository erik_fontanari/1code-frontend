import React, { Component } from 'react';
import {
  Text,Image,
  View, Button, TouchableOpacity, Alert
} from 'react-native';
import { Content } from 'native-base';
import { LocaleConfig } from 'react-native-calendars';
import { Subject } from 'rxjs';
import { filter, debounceTime } from 'rxjs/operators';

import { styles } from './AccessUser-Styles';
import { COMPONENT_SETTINGS } from './AccessUser-Settings';
import { getFormFromThisStep } from './forms-design';

import { STATIC_ASSETS } from '../../../../config/app';
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';
import { THEME_CustomForm } from '../../../../theme/components/form/';
import { doFunctionOnForm } from '../../../sections/forms/Forms-Manager';

export default class AccessUserComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;
  checkFormUpdater = new Subject();

  constructor(props){
    super(props);
  }

  componentDidMount() {

    this.setState({
      localView : this.props.localView
    })

    this.checkFormUpdater.pipe(
      debounceTime(50),
      filter( item => item.step !== this.state.currentViewStep ),
    )
    .subscribe( res => {
      this.setState({
        currentViewStep : res.step
      })
    })
  }

  shouldComponentUpdate(nextProps){
    // console.log("################################")
    // console.log( "currentStep:", nextProps.localView.step, this.state.currentViewStep )
    this.checkFormUpdater.next( nextProps.localView );
    return true
  }

  getCurrentFormName = ( step? : number) : string => {
    return `form_${ this.props.localView.name }_${ ( step ? step : this.props.localView.step ) }`
  }

  receiveCallbackFromForm = ( feedback : any ) : void => {

    let bufferFormsData = this.state.formsData;
    if( feedback.formName === this.getCurrentFormName( 1 ) && feedback.function === "submit" ) {
      bufferFormsData[ `${ 1 }` ] = feedback.content;
      this.props.userAccessManager( feedback.content );
    }

    if( feedback.function === "password_forgot" ) {
      console.log("password_forgot!!!")

      let buff = this.state.localView;
      buff.step = 2;

      this.setState({
       localView : buff
      })
      this.props.updateLocalView( "ACCESS", "NEXT" )
    }

    if( feedback.formName === this.getCurrentFormName( 2 ) && feedback.function === "submit" ) {
      // bufferFormsData[ `${ 1 }` ] = feedback.content;
      Alert.alert( "Email inviata di nuovo :) " )
      doFunctionOnForm( feedback.formName, "form_available" );
    }
    // else if( feedback.formName === this.getCurrentFormName( 2 ) ) {
    //   bufferFormsData[ `${ 2 }` ] = feedback.content;
    // }
    //
    // this.props.updateLocalView( "ACCESS", "NEXT" )
    this.setState({
      formsData : bufferFormsData
    })
  }

  render() {
    let currentStep;
    let printForm;

    if( this.props.localView.name === "access" ) {
      if( this.props.localView.step === this.state.currentViewStep ) {
        let formName = this.getCurrentFormName( this.state.currentViewStep )
        printForm = (
          <THEME_CustomForm
            formName={ formName }
            formSettings={ getFormFromThisStep( formName ) }
            callback={ feedback => this.receiveCallbackFromForm( feedback ) }
          />
        )
      }

      // --- Terms and conditions view -------------------------------------------
      if( this.props.localView.step === 1 ) {
        currentStep = {
          header : {
            logo : STATIC_ASSETS.LOGO_APP_BLUE,
            flex: 0.2
          },
          content : {
            thisView : printForm,
            flex: 0.8
          }
        }
      }

      // --- Step 2 --------------------------------------------------------------
      else if( this.props.localView.step === 2 ) {
        currentStep = {
          header : {
            logo : STATIC_ASSETS.FORGOT_PASS,
            slogan : "Inserisci l’email che utilizzi per fare il login a 1Code e inizia il processo di modifica della password",
            flex: 0.3
          },
          content : {
            thisView : printForm,
            flex: 0.7
          }
        }
      }
    }

    return (
      <THEME_COMPONENTS.VIEW
        body={{
          type: "default",
          ...currentStep
        }}
      />
    );
  }
}

AccessUserComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

AccessUserComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
