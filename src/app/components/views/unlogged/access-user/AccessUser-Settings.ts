import PropTypes from 'prop-types';

interface I__AccessUserState {
  localView : any,
  formsData : any,
  currentViewStep : number
}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {
    localView : {},
    formsData : {
      "1" : {},
      "2" : {}
    },
    currentViewStep : 1
  } as I__AccessUserState,
  PROP_TYPES : {
    localView : PropTypes.shape(),
    updateOn: PropTypes.number,
    updateLocalView : PropTypes.func.isRequired,
    userAccessManager : PropTypes.func.isRequired
  },
  DEFAULT_PROPS : {}
};
