import React, { Component } from 'react';
import { Text } from 'react-native';
import { LocaleConfig } from 'react-native-calendars';
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';
import {
  getCheckBoxSettings,
  getTextSettings,
  getDateSettings,
  getSelectSettings,
  getEmailSettings,
  getPasswordSettings,
  getDefaulDesign
} from "../../../../theme/components/form/proto-design";

export const getFormFromThisStep = ( currentStep : number ) : any => {
  let res = {};

  // // Access credential
  if( currentStep === "form_access_1" ) {
    res = {
      data : [
        getEmailSettings( "email", true, "Email" ),
        getPasswordSettings( "password", "Password", false )
      ],
      design:{
        ...getDefaulDesign({
          title :"Accedi"
        }, {
          password_forgot : {
            label : 'Password dimenticata?',
            designType: 'blank'
          }
        }
        )
      }
    }
  }

  // Password forgotten
  else if( currentStep === "form_access_2" ) {
    res = {
      data : [
        getEmailSettings( "email", true, "Email" ),
      ],
      design:{
        ...getDefaulDesign({
          title :"Invia"
        })
      }
    }
  }

  return res;
}
