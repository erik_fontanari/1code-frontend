import { connect } from 'react-redux';
import AccessUserComponent from './AccessUser-Component';
import { updateLocalView_UnLogged, userAccessManager } from '../../../../store/actions/appActions';

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => ({
  updateLocalView: ( viewType : string, requestType? : string ) => dispatch( updateLocalView_UnLogged( viewType, requestType ) ),
  userAccessManager: ( userData : any ) => dispatch( userAccessManager( userData, "login" ) )
});

const AccessUser = connect( mapStateToProps, mapDispatchToProps )( AccessUserComponent );

export default AccessUser;
