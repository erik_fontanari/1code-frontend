import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { styles } from './HomeSelection-Styles';
import { COMPONENT_SETTINGS } from './HomeSelection-Settings';

import { STATIC_ASSETS, COLOR_PALETTES } from '../../../../config/app';
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';

import SwiperProto from '../../../sections/swiper/';

export default class HomeSelectionComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;

  constructor(props){
    super(props);
  }

  render() {

    let resumersContent = (
      <View style={styles.mainContent}>

        <SwiperProto
          dataSwiper={[
            {
              title : "The world on your wrist",
              slogan : "Carte d’identità, sconti illimitati, carte fedeltà e molto altro in un unico codice"
            },
            {
              title : "The world on your wrist",
              slogan : "Carte d’identità, sconti illimitati, carte fedeltà e molto altro in un unico codice"
            },
            {
              title : "The world on your wrist",
              slogan : "Carte d’identità, sconti illimitati, carte fedeltà e molto altro in un unico codice"
            }
          ]}
          renderItemComponent={ ( item ) => (
            <View style={{ alignItems:'center' }}>
              <Text style={{ ...THEME_STYLES.APP.importantText}}>
                { item.title }
              </Text>
              <Text style={ styles.otherTextSlide }>
                { item.slogan }
              </Text>
            </View>
          ) }
        />

        <View style={styles.buttonsContent}>
          <THEME_COMPONENTS.BUTTON
            designType={"blank"}
            label={"Accedi"}
            onPress={ () => this.props.updateLocalView( "ACCESS" ) }
          />
          <THEME_COMPONENTS.BUTTON
            designType={"main"}
            label={"Iscriviti ora"}
            onPress={ () => this.props.updateLocalView( "REGISTER" ) }
          />
        </View>
      </View>
    );

    return (
      <THEME_COMPONENTS.VIEW
        body={{
          type : "resumeContent",
          header : {
            logo : STATIC_ASSETS.LOGO_APP_BLUE,
            flex: 0.1
          },
          resumers : {
            thisView : resumersContent,
            flex : 0.9
          }
        }}
      />
    );
  }
}

HomeSelectionComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

HomeSelectionComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
