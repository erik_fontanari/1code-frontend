import PropTypes from 'prop-types';

interface I__DetailsEventState {}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {} as I__DetailsEventState,
  PROP_TYPES : {
    updateLocalView : PropTypes.func.isRequired,
  },
  DEFAULT_PROPS : {}
};
