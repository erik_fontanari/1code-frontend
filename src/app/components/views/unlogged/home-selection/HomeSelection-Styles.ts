import { StyleSheet, Dimensions } from 'react-native';
import { THEME_STYLES } from '../../../../theme/';
import { WIDTH_APP, HEIGHT_APP, COLOR_PALETTES } from '../../../../config/app';

export const styles = StyleSheet.create({
  container: {
    height: HEIGHT_APP
  },
  logoContent : {
    width: '100%',
    height:'46%',
    justifyContent:'flex-start',
    margin:0,
    paddingTop:60,
  },
  mainContent : {
    width:'100%',
    height:'50%',
  },
  buttonsContent : {
    alignItems:'center',
    margin:0
  },
  otherTextSlide: {
    ...THEME_STYLES.APP.lightText,
    marginTop:20,
    textAlign:'center'
  }
});
