import { connect } from 'react-redux';
import HomeSelectionComponent from './HomeSelection-Component';
import { updateLocalView_UnLogged } from '../../../../store/actions/appActions';

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => ({
  updateLocalView: ( viewType : string, requestType? : string ) => dispatch( updateLocalView_UnLogged( viewType, requestType ) ),
});

const HomeSelection = connect( mapStateToProps, mapDispatchToProps )( HomeSelectionComponent );

export default HomeSelection;
