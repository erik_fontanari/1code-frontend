import React, { Component } from 'react';
import { Text, View, Button, Alert } from 'react-native';
import { Content } from 'native-base';
import { LocaleConfig } from 'react-native-calendars';
import { Subject } from 'rxjs';
import { filter, debounceTime } from 'rxjs/operators';

import { styles } from './RegisterUser-Styles';
import { COMPONENT_SETTINGS } from './RegisterUser-Settings';
import { getFormFromThisStep } from './forms-design';

import VideoCustom from '../../../sections/video/';

import LoadingProto from '../../../sections/loading/';
import { doFunctionOnForm } from '../../../sections/forms/Forms-Manager';
import { STATIC_ASSETS } from '../../../../config/app';
import { UserExistsOnDB } from '../../../../persisting-data/APIs_manager';
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';
import { THEME_CustomForm } from '../../../../theme/components/form/';

export default class RegisterUserComponent extends Component {

  state = COMPONENT_SETTINGS.INITIAL_STATE;
  checkFormUpdater = new Subject();

  constructor(props){
    super(props);
  }

  componentDidMount() {

    this.setState({
      localView : this.props.localView
    })

    this.checkFormUpdater.pipe(
      debounceTime(50),
      filter( item => item.step !== this.state.currentViewStep ),
    )
    .subscribe( res => {
      this.setState({
        currentViewStep : res.step
      })
    })
  }

  shouldComponentUpdate(nextProps){
    this.checkFormUpdater.next( nextProps.localView );
    return true
  }

  getCurrentFormName = ( step? : number) : string => {
    return `form_${ this.props.localView.name }_${ ( step ? step : this.props.localView.step ) }`
  }

  receiveCallbackFromForm = ( feedback : any ) : void => {

    const goAway = ( bufferFormsData : any ) : void => {
      this.props.updateLocalView( "REGISTER", "NEXT" )
      this.setState({
        formsData : bufferFormsData
      })
    }

    if( feedback.function === "submit" ) {
      let bufferFormsData = this.state.formsData;
      if( feedback.formName === this.getCurrentFormName( 1 ) ) {
        bufferFormsData[ `${ 1 }` ] = feedback.content;
        goAway( bufferFormsData )
      }
      else if( feedback.formName === this.getCurrentFormName( 2 ) ) {
        bufferFormsData[ `${ 2 }` ] = feedback.content;
        goAway( bufferFormsData )
      }
      else if( feedback.formName === this.getCurrentFormName( 3 ) ) {
        UserExistsOnDB( feedback.content.email, feedback.content.nickname ).subscribe( thisUser => {
          if( thisUser.emailNotExists === true && thisUser.nicknameNotExists === true ) {
            bufferFormsData[ `${ 3 }` ] = feedback.content;
            goAway( bufferFormsData )
          } else {
            doFunctionOnForm( feedback.formName, "form_error", "wrong_typing" )
          }
        })
      }
    }
  }

  render() {
    let currentStep;
    let printForm;

    if( this.state.videoView === true ) {
      currentStep = (
        <VideoCustom urlVideo={ "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" } />
      );
    } else {
      if( this.props.localView.name === "registration" ){
        if( this.props.localView.step === this.state.currentViewStep ) {
          let formName = this.getCurrentFormName( this.state.currentViewStep )
          printForm = (
            <THEME_CustomForm
              formName={ formName }
              formSettings={ getFormFromThisStep( formName ) }
              callback={ feedback => this.receiveCallbackFromForm( feedback ) }
            />
          )
        }

        // --- Terms and conditions view -------------------------------------------
        if( this.props.localView.step === 1 ) {
          currentStep = {
            type : "default",
            header : {
              title : "Termini e condizioni",
              logo : STATIC_ASSETS.PRIVACY,
              flex : 0.3
            },
            content : {
              thisView : printForm,
              flex : 0.7
            }
          }
        }

        // --- Step 2 --------------------------------------------------------------
        else if( this.props.localView.step === 2 ) {
          currentStep = {
            type : "default",
            heightScreen : "130%",
            header : {
              title : "I tuoi dati",
              flex : 0.1
            },
            content : {
              thisView : printForm,
              flex : 0.9
            }
          }
        }

        // --- Step 3 --------------------------------------------------------------
        else if( this.props.localView.step === 3 ) {
          currentStep = {
            type : "default",
            header : {
              title : "Accesso e utilizzo",
              flex : 0.1
            },
            content : {
              thisView : printForm,
              flex : 0.9
            }
          }
        }

        // --- Step 4 --------------------------------------------------------------
        else if( this.props.localView.step === 4 ) {
          let headerContent = (
            <View>

              <Text style={ THEME_STYLES.APP.labelFieldFormText }>
                Ti abbiamo inviato un'email all'indirizzo
              </Text>
              <Text style={ THEME_STYLES.APP.mainText }>
                { this.state.formsData[ '3' ].email }
              </Text>

              <Text style={{ ...THEME_STYLES.APP.lightText, paddingTop:20}}>
                Clicca sul link per certificare la tua mail e completare la registrazione.
              </Text>

              <Text style={{ ...THEME_STYLES.APP.lightText, paddingTop:20}}>
                Se non trovi l’email, potrebbe essere finita nella cartella “spam”
              </Text>

            </View>
          );

          let resumersContent = (
            <View>

              <THEME_COMPONENTS.BUTTON
                onPress={()=>{
                  Alert.alert( "Email inviata di nuovo :) " )
                }}
                label={ "Reinvia email" }
                designType={ "blank" }
              />
              <THEME_COMPONENTS.BUTTON
                onPress={()=>{
                  let buff = this.state.localView;
                  buff.step = 5;

                  this.setState({
                    localView : buff
                  })
                }}
                label={ "Esci" }
                designType={ "main" }
              />

            </View>
          );

          currentStep = {
            type : "resumeContent",
            header : {
              title : "Certificazione email",
              logo : STATIC_ASSETS.EMAIL,
              content : headerContent,
              flex : 0.4
            },
            resumers : {
              thisView : resumersContent,
              flex : 0.6
            }
          }
        }

        // --- Step 5 --------------------------------------------------------------
        else if( this.props.localView.step === 5 ) {
          let headerContent = (
            <View>

              <Text style={ THEME_STYLES.APP.labelFieldFormText }>
                Abbiamo completato tutti i passaggi
              </Text>

              <Text style={{ ...THEME_STYLES.APP.lightText }}>
                Ti diamo il benvenuto su 1Code.
              </Text>

              <Text style={{ ...THEME_STYLES.APP.lightText, paddingTop:20, paddingBottom:20 }}>
                Da adesso potrai gestire sconti acquisto, eventi, profili protetti da pin e molto altro in un unico codice
              </Text>

            </View>
          );

          let resumersContent = (
            <View>

              { this.state.finalLoading === false ?
                <View>
                  <THEME_COMPONENTS.BUTTON
                    label={ "Video tutorial" }
                    designType={ "blank" }
                    onPress={()=>{
                      console.log("Video tutorial")
                      // this.setState({
                      //   videoView : true
                      // })
                    }}
                  />
                  <THEME_COMPONENTS.BUTTON
                    label={ "Inizia" }
                    designType={ "main" }
                    onPress={()=>{
                      console.log("Inizia")
                      this.setState({
                        finalLoading : true
                      })
                      let bodyUser = {
                        ...this.state.formsData[ '2' ],
                        ...this.state.formsData[ '3' ]
                      }
                      this.props.userAccessManager( bodyUser );
                    }}
                  />
                </View> :
                <View style={{height:30,alignItems:'center'}}>
                  <LoadingProto graphicalLoading={ false } />
                </View>
              }

            </View>
          );

          currentStep = {
            type : "resumeContent",
            header : {
              title : "Grazie!",
              logo : STATIC_ASSETS.FINISH,
              content : headerContent,
              flex : 0.4
            },
            resumers : {
              thisView : resumersContent,
              flex : 0.6
            }
          }
        }
      }
    }

    return (
      <THEME_COMPONENTS.VIEW body={ currentStep } />
    );
  }
}

RegisterUserComponent.propTypes = COMPONENT_SETTINGS.PROP_TYPES;

RegisterUserComponent.defaultProps = COMPONENT_SETTINGS.DEFAULT_PROPS;
