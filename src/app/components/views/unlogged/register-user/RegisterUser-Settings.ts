import PropTypes from 'prop-types';

interface I__RegisterUserState {
  localView : any,
  formsData : any,
  currentViewStep : number,
  finalLoading : boolean,
  videoView : boolean
}

export const COMPONENT_SETTINGS = {
  INITIAL_STATE : {
    localView : {},
    formsData : {
      "1" : {},
      "2" : {},
      "3" : {}
    },
    currentViewStep : 1,
    finalLoading : false,
    videoView : false
  } as I__RegisterUserState,
  PROP_TYPES : {
    localView : PropTypes.shape(),
    updateOn: PropTypes.number,
    updateLocalView : PropTypes.func.isRequired,
    userAccessManager : PropTypes.func.isRequired
  },
  DEFAULT_PROPS : {}
};
