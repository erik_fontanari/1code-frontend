import { StyleSheet, Dimensions } from 'react-native';
import { THEME_STYLES } from '../../../../theme/';
import { WIDTH_APP, HEIGHT_APP, COLOR_PALETTES } from '../../../../config/app';

export const styles = StyleSheet.create({
  container: {
    flex:1,
    height:'100%'
  },
  logoContent : {
    width: '50%',
    height:'50%',
    paddingTop:60
  },
  mainContent : {
    width:'100%',
    height:'50%'
  },
  sliderContent : {
    width:'100%',
    height:'50%',
    alignItems:'center'
  },
  buttonsContent : {
    height:'40%',
    alignItems:'center',
    justifyContent:'flex-end',
    margin:15
  },
  wrapper: {},
  currentSlide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    ...THEME_STYLES.APP.lightContent,
  },
  otherTextSlide: {
    ...THEME_STYLES.APP.lightText,
    margin:50,
    marginTop:20,
    textAlign:'center'
  }
});
