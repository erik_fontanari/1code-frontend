import React, { Component } from 'react';
import { Text } from 'react-native';
import { LocaleConfig } from 'react-native-calendars';
import { THEME_STYLES, THEME_COMPONENTS } from '../../../../theme/';
import {
  getCheckBoxSettings,
  getTextSettings,
  getDateSettings,
  getSelectSettings,
  getEmailSettings,
  getPasswordSettings,
  getDefaulDesign
} from "../../../../theme/components/form/proto-design";

export const getFormFromThisStep = ( currentStep : number ) : any => {
  let res = {};

  // Terms and conditions
  if( currentStep === "form_registration_1" ) {
    res = {
      data : [
        getCheckBoxSettings( "treat_agreement", () => (
          <Text style={{width:'100%', ...THEME_STYLES.APP.labelFormText }}>
            Acconsento al trattamento dei miei dati per le
            finalità di funzionamento dell’App 1Code, per la
            per le iniziative promozionali dei partner.
          </Text>
        ) ),
        getCheckBoxSettings( "privacy_policy", () => (
          <Text style={{width:'100%', ...THEME_STYLES.APP.labelFormText }}>
            Dichiaro di aver letto e accettato la
            <THEME_COMPONENTS.LINK
              label={ "Privacy policy" }
              url={ "http://google.com" }
            />.
          </Text>
        ) ),
        getCheckBoxSettings( "license_conditions", () => (
          <Text style={{width:'100%', ...THEME_STYLES.APP.labelFormText }}>
            Dichiaro di aver letto e accettato le
            <THEME_COMPONENTS.LINK
              label={ "Condizioni di licenza" }
              url={ "http://google.com" }
            />
            e il regolamento.
          </Text>
        ) )
      ],
      design:{
        ...getDefaulDesign({
          title :"Accetto"
        })
      }
    }
  }

  // User data
  else if( currentStep === "form_registration_2" ) {
    res = {
      data : [
        getTextSettings( "nome", "es: Mario", "Nome" ),
        getTextSettings( "cognome", "es: Rossi", "Cognome" ),
        getSelectSettings( "gender", [
          { value: 'M', label: "Maschio" },
          { value: 'F', label: "Femmina" }
        ], "Uomo, Donna", "Sesso"),
        getDateSettings( "data_nascita", "dd/mm/yyyy", "Data di nascita" ),
        getSelectSettings( "luogo_nascita", LocaleConfig.locales['it'].provinceIta, "es: Milano", "Luogo di nascita"),
        getSelectSettings( "luogo_residenza", LocaleConfig.locales['it'].provinceIta, "es: Milano", "Luogo di residenza")
      ],
      design:{
        ...getDefaulDesign({
          title :"Continua"
        })
      }
    }
  }

  // Access credential
  else if( currentStep === "form_registration_3" ) {
    res = {
      data : [
        getEmailSettings( "email", true, "Email" ),
        getTextSettings( "nickname", "es: nome.cognome", "Nickname" ),
        getPasswordSettings( "password", "Password", true )
      ],
      design:{
        ...getDefaulDesign({
          title :"Continua"
        })
      }
    }
  }

  return res;
}
