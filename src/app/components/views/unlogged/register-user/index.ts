import { connect } from 'react-redux';
import RegisterUserComponent from './RegisterUser-Component';
import { updateLocalView_UnLogged, userAccessManager } from '../../../../store/actions/appActions';

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => ({
  updateLocalView: ( viewType : string, requestType? : string ) => dispatch( updateLocalView_UnLogged( viewType, requestType ) ),
  userAccessManager: ( bodyUser : any ) => dispatch( userAccessManager( bodyUser, "register" ) )
});

const RegisterUser = connect( mapStateToProps, mapDispatchToProps )( RegisterUserComponent );

export default RegisterUser;
