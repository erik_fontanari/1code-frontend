import { Dimensions, Platform } from 'react-native';
import { OVERWRITE_CONFIGS } from './overwrite';
import { FIREBASE_INIT } from '../persisting-data/firebase';

// Immagini in locale
import logoApp from '../../assets/img/logo_splash_white.svg';
import logoAppBlue from '../../assets/img/logo_blu_top.svg';
import privacy from '../../assets/img/privacy_top.svg';
import emailTop from '../../assets/img/email_top.svg';
import finishTop from '../../assets/img/finish_top.svg';
import forgotPass from '../../assets/img/forgot_pass_top.svg';
import checkboxOff from '../../assets/img/icon_check_off.svg';
import checkboxOn from '../../assets/img/icon_check_on.svg';
import iconBack from '../../assets/img/icon_arrow_back.svg';
import iconDown from '../../assets/img/icon_arrow_down.svg';
import eyeOpen from '../../assets/img/icon_eye_open.svg';
import eyeClose from '../../assets/img/icon_eye_close.svg';

export const WIDTH_APP    = Dimensions.get( 'window' ).width;
export const HEIGHT_APP   = Dimensions.get( 'window' ).height;

export const COLOR_PALETTES = {
  purple     : "#841584",
  lightblue  : "#3D83FE",
  normalgrey : "#919291",
  darkblue   : "#210E4A",
  lightgrey  : "#EEF4FF",
  white      : "#FFFFFF",
  black      : "#000000"
}

// Device informations, esempio PLATFORM_APP.OS === 'android' || ios
export const PLATFORM_APP = Platform;

export const VIEWS_SETTINGS = {
  "INIT" : {
    name : "init",
    navbar : false,
    voice : "INIT"
  },
  "REGISTER" : {
    steps : 5,
    name : "registration",
    navbar : {
      titles : "Registrazione"
    },
    voice : "REGISTER"
  },
  "ACCESS" : {
    steps : 3,
    name : "access",
    navbar : {
      titles : [ "Accedi", "Password dimenticata" ]
    },
    voice : "ACCESS"
  },
  "HOME_LOGGED" : {
    steps : 1,
    name : "home_logged",
    navbar : false,
    voice : "HOME_LOGGED"
  }
}

export const INITIAL_APP_STATE = {
  user : undefined,
  localView : VIEWS_SETTINGS.INIT
}

export const STATIC_ASSETS = {
  LOGO_APP : logoApp,
  LOGO_APP_BLUE : logoAppBlue,
  PRIVACY : privacy,
  EMAIL : emailTop,
  FINISH : finishTop,
  FORGOT_PASS : forgotPass,
  CHECKBOX_OFF : checkboxOff,
  CHECKBOX_ON : checkboxOn,
  ARROW_BACK : iconBack,
  ARROW_DOWN : iconDown,
  ARROW_DOWN : iconDown,
  EYE_OPEN : eyeOpen,
  EYE_CLOSE : eyeClose,
  PLACEHOLDER_IMG : "https://firebasestorage.googleapis.com/v0/b/onecode-production-firebase.appspot.com/o/user_icon.svg?alt=media&token=2cd7e61d-99d4-499f-a6e5-c6369ea611ee"
}

export const INIT_CONFIGS_LOADER = () : void => {
  FIREBASE_INIT();
  OVERWRITE_CONFIGS( );
}
