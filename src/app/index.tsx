
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { INIT_CONFIGS_LOADER } from './config/app';
import App from './components/app/';
import store from './store/';

INIT_CONFIGS_LOADER();

export default class MainContent extends Component {

  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    )
  }
}
