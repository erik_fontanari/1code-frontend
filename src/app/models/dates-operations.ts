import { LocaleConfig } from 'react-native-calendars';

// GET: 2 Giu 2020
export const getFormattedDate = ( currentDate ) : string => {
  let res = "";
  let d = new Date( currentDate );
  res += `${ ( d.getDate() ) }`;
  res += ` ${ LocaleConfig.locales['it'].monthNamesShort[( d.getMonth() )] }`;
  res += ` ${ ( d.getFullYear() ) }`;
  return res;
}

// GET: needTZ = false => "2020-05-02 00:00:00"
// GET: needTZ = true  => "2020-05-02T00:00:00.000Z"
export const getEntireDate = ( k, needTZ = false ) : string => {
  let d = new Date( k );
  let res = `${ ( d.getFullYear() ) }-`;
  let month = ( d.getMonth() + 1 );
  let day = d.getDate();
  let completeHours = `${ ( needTZ === true ? "T" : " " ) }00:00:00${ ( needTZ === true ? ".000Z" : "" ) }`;
  res += ( month <= 9 ? `0${ month }` : `${ month }` ) + '-' + ( day <= 9 ? `0${ day }` : `${ day }` ) + completeHours
  return res;
}

// Set a date ( y-m-d 00:00:00 ) and get its timestamp
export const getTimeStampAtMidnight = ( k ) : number => {
  let d = new Date( k );
  return ( new Date( d.getFullYear(), d.getMonth(), d.getDate(), 0,0,0,0 ) ).getTime();
}

// export const getDateFromTimestamp = ( timeStamp : number ) : Date => {
//   return ( new Date( timeStamp * 1000 ) )
// }


// // import moment from 'moment';
// import * as moment from 'moment';
//
// type newDate = moment.Moment;
//
// export interface I__currentDate {
//   dateStr : string;
//   timestamp : number;
// }
// export interface I__TolleranceMonths {
//   min : I__currentDate;
//   max : I__currentDate;
// }
// export const getTimeStamp = ( stringDate : string ) : number => {
//   return ( new Date ( stringDate ) ).getTime();
// }
// export const getCurrentDate = ( thisDate : string ) : I__currentDate => {
//   return {
//     dateStr : thisDate,
//     timestamp : getTimeStamp( thisDate )
//   }
// }
// export const getMoment = ( currentDate? : string ) : newDate => {
//   return ( !currentDate ? moment() : moment( currentDate ) ) as newDate;
// }
// export const getTolleranceMonths = ( monthTollerance : number, currentDate? : string ) : I__TolleranceMonths => {
//   let begin : string = getMoment( currentDate ).startOf('month').subtract(monthTollerance,'month').format();
//   let end : string = getMoment( currentDate ).endOf('month').add(monthTollerance,'month').format();
//
//   return {
//     min : getCurrentDate( begin ),
//     max : getCurrentDate( end )
//   } as I__TolleranceMonths
// }
// export const getMonthBeginDate = ( currentDate : string ) : I__currentDate => {
//   let beginOfMonth : string = getMoment( currentDate ).startOf('month').format();
//   return getCurrentDate( beginOfMonth );
// }
// export const addMonth = ( currentDate : string, numbMonths : number ) : I__currentDate => {
//   let addedMonth : string = getMoment( currentDate ).startOf('month').add(numbMonths,'month').format();
//   return getCurrentDate( addedMonth );
// }
// // export const listOfTimeStamps = ( currentStart : string, currentEnd : string ) : any[] => {
// //   let listTimeStamps = [];
// //
// //   let begin = getMonthBeginDate( currentStart );
// //   let end = getMonthBeginDate( currentEnd );
// //   console.log(begin, end)
// //   let addedMonth = 0;
// //   const addTimeStamp = () : void => {
// //     let newMonth = addMonth( currentStart, addedMonth );
// //     if( newMonth.timestamp >= begin.timestamp && newMonth.timestamp < end.timestamp ) {
// //       listTimeStamps.push( newMonth.timestamp );
// //       addedMonth++;
// //       addTimeStamp();
// //     }
// //   }
// //   addTimeStamp();
// //
// //   return listTimeStamps;
// // }
//
// export const listOfTimeStamps = ( currentStart : string, currentEnd : string ) : number[] => {
//   let end = getMonthBeginDate( currentEnd );
//   let listCompleted = false;
//   let list = [] as number[];
//   let index = 0;
//   while( listCompleted == false ) {
//     let newMonth = addMonth( currentStart, index++ );
//     if( newMonth.timestamp <= end.timestamp ) {
//       list.push( newMonth.timestamp )
//     } else {
//       listCompleted = true;
//     }
//   }
//   return list;
// }
