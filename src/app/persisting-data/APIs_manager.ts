import axios from "axios";
import { Subject, Observable } from 'rxjs';
import { I__ResponseData, API_URL } from './shared';

let headers = {
  'Accept': 'application/json, text/plain, */*',
  'Content-Type': 'application/json'
  // 'key':'xxxxxxxx',
};
const SECRET_KEY = '7d0891a6ce293f6ff23bd51005177486';

export const REQUESTS_SERVER = {
  HOME : "/",
  CREATE_ACCOUNT : "/profile/create_account",
  NICKNAME_ALREADY_EXISTS : "/profile/does_nickname_already_exist",
  EMAIL_ALREADY_EXISTS : "/profile/does_email_already_exist",
}

export const API_CALL = ( inputReq : string, dataContent? : any ) : Observable<I__ResponseData> => {
  let res = new Subject<I__ResponseData>();

  axios.post( `${API_URL}${inputReq}`, dataContent )
  .then( response => {
    res.next({
      data : response.data
    })
  })
  .catch( error => {
    res.next({
      error : error
    })
  });

  return res.asObservable();
}


// --- BASIC API CALL ----------------------------------------------------------
export const UserExistsOnDB = ( email : string, nickname : string ) : Observable<any> => {
  let res = new Subject<I__ResponseData>();
  API_CALL( REQUESTS_SERVER.EMAIL_ALREADY_EXISTS, {email : email } ).subscribe( emailExists => {
    API_CALL( REQUESTS_SERVER.NICKNAME_ALREADY_EXISTS, {nickname : nickname } ).subscribe( nicknameExists => {
      let email = ( emailExists.data.success === true && emailExists.data.data === false )
      let nickname = ( nicknameExists.data.success === true && nicknameExists.data.data === false )
      res.next({ emailNotExists : email, nicknameNotExists : nickname });
      res.complete();
    })
  })
  return res.asObservable();
}
