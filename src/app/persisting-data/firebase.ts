import { Subject, Observable } from 'rxjs';
import * as firebase from "firebase";
import { firebaseConfig } from "./shared";
//
export const FIREBASE_INIT = () : void => {
  firebase.initializeApp(firebaseConfig);
}

const CONNECTION = () : any => {
  return firebase.app()
}

export const userOnFirebase = ( email : string, password : string, mode : string ) : any => {
  let thisUser = new Subject();
  const sendData = ( inputBody : any ) : void => {
    thisUser.next( inputBody )
    thisUser.complete( )
  }

  let req;
  if( mode === "register" ){
    req = CONNECTION().auth().createUserWithEmailAndPassword( email, password );
  }
  else if( mode === "login" ){
    req = CONNECTION().auth().signInWithEmailAndPassword( email, password );
  }

  req
  .then( data => {

    CONNECTION().auth().currentUser.getIdToken(/* forceRefresh */ false).then( idToken => {
      sendData({
        user : data,
        idToken : idToken
      })
    }).catch( (error) => {
      sendData({
        user : data,
        idToken : error
      })
    });

  })
  .catch( error => {
    sendData({
      error : error
    })
  });

  return thisUser.asObservable();
}
