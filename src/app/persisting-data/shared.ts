export interface I__ResponseData {
  error? : any;
  data? : any;
}

export const API_URL = 'https://onecode-testing.appspot.com';

// testing
export const firebaseConfig = {
  apiKey: "AIzaSyC6lplulqQEaa8D492rXbTwxvvongYLOpE",
  authDomain: "onecode-testing-firebase.firebaseapp.com",
  databaseURL: "https://onecode-testing-firebase.firebaseio.com",
  projectId: "onecode-testing-firebase",
  storageBucket: "onecode-testing-firebase.appspot.com",
  messagingSenderId: "633076279452",
  appId: "1:633076279452:web:c3607f681d57902faf2559",
  measurementId: "G-GP3FFPC32H"
};

// production
// export const firebaseConfig = {
//   apiKey: "AIzaSyDKq4pAI0X2gQ8-GLrhKCf9n9uG8QoOAVk",
//   authDomain: "onecode-production-firebase.firebaseapp.com",
//   databaseURL: "https://onecode-production-firebase.firebaseio.com",
//   projectId: "onecode-production-firebase",
//   storageBucket: "onecode-production-firebase.appspot.com",
//   messagingSenderId: "824753161915",
//   appId: "1:824753161915:web:03eab442bbe2afa9f38b9e",
//   measurementId: "G-58XXMQGCQM"
// };
