import { API_CALL, REQUESTS_SERVER, UserExistsOnDB } from '../../persisting-data/APIs_manager';
import { /* verifyIdTokenOnFirebase */ userOnFirebase } from '../../persisting-data/firebase';
import { VIEWS_SETTINGS, STATIC_ASSETS } from '../../config/app';
import { getEntireDate } from '../../models/dates-operations';
import { doFunctionOnForm } from '../../components/sections/forms/Forms-Manager';

export const ACTIONS_MAPPERS = {
  'GET_ALL'            : "[ appData ] : Get all settings from redux",
  'UPDATE_VIEW'        : "[ appData ] : Update view on routing",
  'LOG_IN_USER'        : "[ appData ] : Login current user"
};

export const getAllData = data => {
  return( dispatch ) => {
    // #TMP:
    // qui poi ci andrà l'auto log utente
    setTimeout( () => {
      // fetchFonts().then( y => {
        dispatch({
          type: ACTIONS_MAPPERS[ 'GET_ALL' ],
          payload : {
            user : {
              logged: false
            },
            localView : VIEWS_SETTINGS.INIT
          }
        })
      // })
    }, 2000)
  }
}

export const updateLocalView_UnLogged = ( viewType : string, requestType? : string ) => dispatch => {
  let payload = {};
  let typeDispatch = "GET_ALL";

  if( viewType === "INIT" ) {
    payload = {
      localView : VIEWS_SETTINGS.INIT
    }
  }
  if( viewType === "REGISTER" || viewType === "ACCESS" ) {
    payload = {
      localView : {
        name : VIEWS_SETTINGS[ viewType ].name,
        navbar : VIEWS_SETTINGS[ viewType ].navbar,
        voice : VIEWS_SETTINGS[ viewType ].voice,
        step : 1
      }
    }
  }

  if( requestType !== undefined ) { // BACK o NEXT
    typeDispatch = "UPDATE_VIEW";
    payload = {
      name : viewType,
      requestType : requestType
    }
  }

  dispatch({
    type: ACTIONS_MAPPERS[ typeDispatch ],
    payload : payload
  });
}

export const userAccessManager = ( data : any, mode : string ) => {

  return( dispatch ) => {

    const dispatchThisUser = ( userData : any ) : void => {

      console.log("userData:", userData)

      dispatch({
        type: ACTIONS_MAPPERS[ 'LOG_IN_USER' ],
        payload : {
          user : {
            // ... other User Data
            logged: true
          },
          localView : VIEWS_SETTINGS.HOME_LOGGED
        }
      })
    }

    if( mode === "register" ) {
      // #FIXME :
      // DISABILITATO IN ATTESA DI CORRETTO FUNZIONAMENTO BACKEND
      console.log( data.email, data.password, mode )
      userOnFirebase( data.email, data.password, mode ).subscribe( thisUser => {
        if( thisUser.idToken != undefined ) {
          // console.log("thisUser:", {
          //   profile_image_url : STATIC_ASSETS.PLACEHOLDER_IMG,
          //   nickname : data.nickname,
          //   lastname : data.cognome,
          //   firstname : data.nome,
          //   firebase_idToken : thisUser.idToken,
          //   email : data.email,
          //   birthday : getEntireDate( data.data_nascita, true ),
          //   birth_place : `${ data.luogo_nascita }`,
          //   gender : `${ data.gender }`,
          //   residence_city : data.luogo_residenza
          // })


          API_CALL( REQUESTS_SERVER.CREATE_ACCOUNT, {
            profile_image_url : STATIC_ASSETS.PLACEHOLDER_IMG,
            nickname : data.nickname,
            lastname : data.cognome,
            firstname : data.nome,
            firebase_idToken : thisUser.idToken,
            email : data.email,
            birthday : getEntireDate( data.data_nascita, true ),
            birth_place : `${ data.luogo_nascita }`,
            gender : `${ data.gender }`,
            residence_city : data.luogo_residenza
          } ).subscribe( userRegistered => dispatchThisUser( userRegistered ) )
        }
      } )

    } else if( mode === "login" ) {
      console.log("login")
      // TMP
      dispatchThisUser( {} )
    }

  }
}

export const settingsModalConfig = ( settings : any ) => dispatch => {

  let newConfig = {
    settingsModalData : settings
  };
  if( settings.modalVisible == false ) {
    newConfig.loopContentData = undefined
  }

  dispatch({
    type: ACTIONS_MAPPERS[ 'GET_ALL' ],
    payload : newConfig
  });
}
