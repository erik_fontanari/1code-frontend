import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import loadingMiddleware from 'redux-loading-middleware';
import reducers from './reducers';

export const store = createStore(
  reducers,
  composeWithDevTools(
    applyMiddleware(loadingMiddleware, thunk)
  ),
);

export default store;
