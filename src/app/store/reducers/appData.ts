import { ACTIONS_MAPPERS } from '../actions/appActions';
import { INITIAL_APP_STATE, /*ROUTE_VIEWS,*/ VIEWS_SETTINGS } from '../../config/app';

const appData = ( state = INITIAL_APP_STATE, action ) => {

	switch ( action.type ) {
    case ACTIONS_MAPPERS[ 'GET_ALL' ]:
			var obj = {
		    ...state,
		    ...action.payload
			};
    case ACTIONS_MAPPERS[ 'LOG_IN_USER' ]:
			var obj = {
		    ...state,
		    ...action.payload
			};

      return obj
    case ACTIONS_MAPPERS[ 'UPDATE_VIEW' ]:
			let currentView = VIEWS_SETTINGS[ action.payload.name ];
			let currentStateView = state.localView;

			if( currentView.name === currentStateView.name ) {
				if( action.payload.requestType === "BACK" ) {
					currentStateView.step = ( currentStateView.step > 1 ? ( currentStateView.step - 1 ) : 1 )
				}
				else if( action.payload.requestType === "NEXT" ) {
					let tryStep = ( currentStateView.step + 1 );
					if( tryStep < currentView.steps ) {
						currentStateView.step = tryStep
					}
				}
			} else {
				currentStateView = {
					name : currentView.name,
					navbar : currentView.navbar,
					step : 1
				}
			}

			return {
				...state,
				localView : currentStateView
			}
    default:
    	return state;
  }
};

export default appData;
