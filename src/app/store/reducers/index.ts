import { combineReducers } from 'redux';
import loading from 'redux-loading-middleware/loadingReducer';
import appData from './appData';

const reducers = combineReducers({
  appData,
  loading
});

export default reducers;
