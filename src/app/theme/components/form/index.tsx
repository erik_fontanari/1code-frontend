import React, { Component } from 'react';
import { View } from 'react-native';
import FormsProto from '../../../components/sections/forms/forms-proto-component/';
import { thisFormListener, doFunctionOnForm } from '../../../components/sections/forms/Forms-Manager';

export class THEME_CustomForm extends Component {
  subscription;

  constructor(props){
    super(props);
  }

  componentDidMount() {
    // if( this.props.formName !== "form_registration_3" ) {
    //   doFunctionOnForm( this.props.formName, "form_init", getFormFromThisStep( this.props.formName ) )
    // }
    this.subscription = thisFormListener( this.props.formName ).subscribe( formContent => {

      // parse formFunction
      if( formContent.formFunction ) {

        this.props.callback({
          formName : this.props.formName,
          content : formContent.functionContent,
          function : ( ( formContent.formFunction ).split( "form_" ) )[ 1 ]
        })
      }
    })
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  render() {
    return (
      <FormsProto
        formName={ this.props.formName }
        formSettingsBuffer={ this.props.formSettings }
        // formSettingsBuffer={ getFormFromThisStep( this.props.formName ) }
      />
    )
  }
}
