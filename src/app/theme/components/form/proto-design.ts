export const getCheckBoxSettings = ( fieldID : string, content : any ) : any => {
  return {
    id : fieldID,
    type : 'checkbox',
    val : false,
    validation: {
      required : true
    },
    style : {
      width:'100%'
    },
    content : content
  }
}

export const getTextSettings = ( fieldID : string, placeholder : string , label : string ) : any => {
  return {
    id : fieldID,
    type : 'text',
    val : '',
    placeholder : placeholder,
    labelField : label,
    validation: {
      required : true
    },
    style : {
      width:'100%'
    }
  }
}

export const getEmailSettings = ( fieldID : string, placeholder : boolean , label : string ) : any => {
  return {
    id : fieldID,
    type : 'email',
    val : '',
    placeholder : placeholder,
    labelField : label,
    validation: {
      required : true
    },
    style : {
      width:'100%'
    }
  }
}

export const getPasswordSettings = ( fieldID : string, label : string, confirm : boolean ) : any => {
  return {
    id : fieldID,
    type : 'password',
    val : '',
    labelField : label,
    validation: {
      required : true,
      confirm : confirm
    },
    style : {
      width:'100%'
    }
  }
}

export const getSelectSettings = ( fieldID : string, dataSet : any[], placeholder : string , label : string ) : any => {
  return {
    id : fieldID,
    type : 'default_select_picker',
    val : 2,
    dataSet : dataSet,
    placeholder : placeholder,
    labelField : label,
    validation: {
      required : true
    },
    style : {
      width:'100%'
    }
  }
}

export const getDateSettings = ( fieldID : string, placeholder : string , label : string ) : any => {
  return {
    id : fieldID,
    val : ( new Date() ),
    type : 'default_date_picker',
    // placeholder : placeholder,
    validation: {
      required : true
    },
    labelField : label,
    style : {
      width:'100%'
    }
  }
}

export const getDefaulDesign = ( inputSubmit : any, otherFunctions? = {} ) : any => {
  return {
    title : "",
    addsOn : {
      functions : {
        submit : inputSubmit,
        delete : false
      },
      otherFunctions : otherFunctions
    }
  }
}
