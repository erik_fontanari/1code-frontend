import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Linking } from 'react-native';
import Imgs from '../../../components/sections/imgs/';
import { appStyles } from '../../styles/app';

export class THEME_Img extends Component {

  constructor( props ){
    super( props );
  }

  render(){
    const { props } = this;
    return (
      <Imgs
        imgType={ props.imgType === "top" ? "topScreenView" : props.imgType }
        thisImg={ props.thisImg }
      />
    )
  }
}
