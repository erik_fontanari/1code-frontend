import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Linking } from 'react-native';
import { appStyles } from '../../styles/app';

export class THEME_Button extends Component {

  constructor( props ){
    super( props );
  }

  render(){
    const { props } = this;
    return (
      <TouchableOpacity
        disabled={ ( props.designType === "disabled" ) }
        style={ appStyles[ `${ props.designType }Button` ] }
        elevation={5}
        onPress={ props.onPress }>
        <Text style={ appStyles[ `${ props.designType }ButtonText` ] }>
          { props.label }
        </Text>
      </TouchableOpacity>
    )
  }
}

export class THEME_Link extends Component {

  constructor( props ){
    super( props );
  }

  render(){
    const { props } = this;
    return (
      <Text
        style={ appStyles.mainText }
        onPress={() => Linking.openURL( props.url )}>
        { ` ${ props.label } ` }
      </Text>
    )
  }
}
