import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { appStyles } from '../../styles/app';

export class THEME_View extends Component {

  constructor( props ){
    super( props );
  }

  render(){
    const { body } = this.props;

    let thisView;

    // ### DEFAULT VIEW DESIGN #################################################
    const defaultView = ( ) => (
      <View style={{flex:1,flexDirection:'column',flexWrap:'wrap',width:'100%',margin:0,padding:0}}>

        { /* --- HEADER ----------------------------------------------------*/ }
        <View style={{...appStyles.defaultScreenContent,paddingBottom:27, width:'100%',flex:body.header.flex}}>
          { body.header.title &&
            <Text style={{ ...appStyles.mainTitle}}>
              { body.header.title }
            </Text>
          }

          { body.header.logo &&
            <View style={{alignItems:'center',marginTop:27,marginBottom:33}}>
              <body.header.logo height={100} width={175} />
            </View>
          }

          { body.header.slogan &&
            <Text style={{ ...appStyles.lightText,textAlign:'left'}}>
              { body.header.slogan }
            </Text>
          }
        </View>

        { /* --- CONTENT ---------------------------------------------------*/ }
        <View style={{...appStyles.defaultScreenContent,width:'100%',flex:body.content.flex}}>
          { body.content.thisView }
        </View>
      </View>
    );


    // ### RESUME CONTENT VIEW DESIGN ##########################################
    const resumeContentView = ( ) => (
      <View style={{flex:1,flexDirection:'column',flexWrap:'wrap',width:'100%',margin:0,padding:0}}>

        { /* --- HEADER ----------------------------------------------------*/ }
        <View style={{...appStyles.defaultScreenContent,paddingBottom:27, width:'100%',flex:body.header.flex}}>
          { body.header.title &&
            <Text style={{ ...appStyles.mainTitle}}>
              { body.header.title }
            </Text>
          }

          { body.header.logo &&
            <View style={{alignItems:'center',marginTop:27,marginBottom:33}}>
              <body.header.logo height={100} width={175} />
            </View>
          }

          { body.header.content &&
            <View style={{justifyContent:'flex-start',width:'100%'}}>
              { body.header.content }
            </View>
          }
        </View>

        { /* --- RESUMERS --------------------------------------------------*/ }
        <View style={{...appStyles.defaultScreenContent,width:'100%',flex:body.resumers.flex, justifyContent:'flex-end'}}>
          { body.resumers.thisView }
        </View>
      </View>
    );

    if( body.type === "default" ) {
      thisView = defaultView();
    }

    if( body.type === "resumeContent" ) {
      thisView = resumeContentView();
    }

    if( body.heightScreen ) {
      thisView = (
        <ScrollView>
          { thisView }
        </ScrollView>
      )
    }

    return (
      <View style={{
        height: ( body.heightScreen ? body.heightScreen : "100%" ),
        width:'100%',
        margin:0,
        padding:0,
        flex:1,
      }}>

      { thisView }

      </View>
    )
  }
}
