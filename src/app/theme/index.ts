import { appStyles } from './styles/app';
import { THEME_Button, THEME_Link } from './components/touchable/';
import { THEME_View } from './components/views/';
import { THEME_Img } from './components/static/img';

export const THEME_STYLES = {
  APP : appStyles
}

export const THEME_COMPONENTS = {
  BUTTON : THEME_Button,
  LINK   : THEME_Link,
  VIEW   : THEME_View,
  IMG    : THEME_Img
}
