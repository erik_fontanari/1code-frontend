import { StyleSheet, Dimensions } from 'react-native';
import { WIDTH_APP, HEIGHT_APP, COLOR_PALETTES } from '../../config/app';

const PROTO_BUTTON = ( needRadius : boolean ) => {

  let proto = {};
  if( needRadius ) {
    proto.borderRadius = 30;
  }

  return {
    width: '100%',
    height:44,
    alignItems:'center',
    justifyContent: 'center',
    margin:0,
    marginTop: 15,
    shadowColor: COLOR_PALETTES.white,
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1
    },
    ...proto
  }
};

const PROTO_BUTTON_TEXT = () => {
  return {
    fontFamily: "BrandonGrotesque-Medium",
    fontSize: 19
  }
}

export const appStyles = StyleSheet.create({
  container: {
    width : WIDTH_APP,
    height : HEIGHT_APP
  },

  // --- Containers ------------------------------------------------------------
  mainContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor : COLOR_PALETTES.lightblue
  },
  lightContent: {
    backgroundColor : COLOR_PALETTES.white
  },
  blueContent: {
    backgroundColor : COLOR_PALETTES.lightblue
  },
  defaultScreenContent: {
    paddingLeft: 20,
    paddingRight: 20
  },

  // --- Selectors -------------------------------------------------------------

  // --- Buttons ---------------------------------------------------------------
  mainButton : {
    ...PROTO_BUTTON( true ),
    backgroundColor: COLOR_PALETTES.lightblue,
    borderRadius: 30
  },
  mainButtonText : {
    ...PROTO_BUTTON_TEXT(),
    color : COLOR_PALETTES.white
  },

  blankButton : {
    ...PROTO_BUTTON( false ),
    backgroundColor : COLOR_PALETTES.white,
  },
  blankButtonText : {
    ...PROTO_BUTTON_TEXT(),
    color : COLOR_PALETTES.lightblue
  },

  disabledButton : {
    ...PROTO_BUTTON( true ),
    backgroundColor : COLOR_PALETTES.lightgrey,
  },
  disabledButtonText : {
    ...PROTO_BUTTON_TEXT(),
    color : COLOR_PALETTES.lightblue
  },

  // --- Texts -----------------------------------------------------------------
  mainText : {
    color : COLOR_PALETTES.lightblue,
    fontSize: 17,
    fontFamily:"BrandonGrotesque-Medium"
  },
  importantText : {
    color : COLOR_PALETTES.black,
    fontSize: 24,
    fontFamily:"BrandonGrotesque-Medium"
  },
  lightText : {
    color : COLOR_PALETTES.normalgrey,
    fontSize: 17,
    fontFamily:"BrandonGrotesque-Light"
  },
  darkText : {
    color : COLOR_PALETTES.darkblue,
    fontSize:19,
    fontFamily:"BrandonGrotesque-Medium"
  },

  // --- Titles ----------------------------------------------------------------
  mainTitle : {
    color : COLOR_PALETTES.lightblue,
    fontSize:24,
    fontFamily:"BrandonGrotesque-Medium",
    textAlign:'center',
    marginTop:33
  },
  labelFieldFormText : {
    color : COLOR_PALETTES.black,
    fontSize: 17,
    fontFamily:"BrandonGrotesque-Medium"
  },

  // --- Forms -----------------------------------------------------------------
  inputFormText : {
    color : COLOR_PALETTES.lightblue,
    fontSize: 17,
    fontFamily:"BrandonGrotesque-Light"
  },
  labelFormText : {
    color : COLOR_PALETTES.black,
    fontSize: 17,
    fontFamily:"BrandonGrotesque-Light"
  }
});
